<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return redirect()->route('dashboard.index');
})->middleware('auth');


Route::Group(['prefix' => 'dashboard', 'namespace' => 'Dashboard', 'middleware' => 'auth'], function () {

    route::get('/index', 'HomeController@index')->name('dashboard.index')->middleware('auth');


    //============= Provider Routes
    Route::resource('providers', 'ProviderController');

//    Route::Group([ 'middleware' => 'permissions'], function () {

        //============= Client Routes
        Route::resource('users', 'UserController');

        //============= Admin Routes
        Route::resource('admins', 'AdminController');


        //================ Notifications Routes =========
        Route::post('send/notify', 'NotificationController@sendNotify')->name('send_notify');


        //================ Address Routes =========
        Route::resource('address', 'AddressController');

        //==============City Routes
        Route::resource('city', 'CityController');


        //===============Category Routes
        Route::resource('categories', 'CategoryController');


        //============================= Products Routs ================================================
        Route::resource('products', 'ProductController');


        //=============== Advertise Routs==========================================
        Route::resource('advertises', 'AdvertisesController');


        //===============Settings Routs ============================================
        Route::get('settings', 'SettingController@index')->name('settings.index'); //
        Route::get('settings/update', 'SettingController@edit')->name('settings.edit');
        Route::post('settings', 'SettingController@update')->name('settings.update');

//});

    //=============================== Orders =====================================
    Route::resource('orders', 'OrderController');
    Route::get('orders/process/{order}', 'OrderController@process')->name('process.order');
    Route::get('orders/ship/{order}', 'OrderController@ship')->name('ship.order');
    Route::get('orders/deliver/{order}', 'OrderController@deliver')->name('deliver.order');


});


