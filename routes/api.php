<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {


    // ================================ User Routes ====================
    Route::any('/register', 'AuthController@Register');
    Route::any('/login', 'AuthController@login');
    Route::any('/active/user/sms', 'AuthController@ActivationAuthSms');

    Route::any('/reset-password-page', 'AuthController@ResetPassword'); ////
    Route::any('/reset-password', 'AuthController@DoResetPassword');  ////


    // ================================ Home Routes ====================
    Route::group(['prefix' => 'home'], function () {
        Route::any('/', 'HomeController@index'); //home
        Route::any('/categories', 'HomeController@categories');
        Route::any('/slider', 'HomeController@slider'); //home
        Route::any('search', 'HomeController@search'); //home

    });


    // ================================ Products Routes ====================
    Route::any('/product', 'ProductController@show');  //Product  Deatils


    Route::group(['middleware' => 'jwt.auth'], function () {

        // ================================Client Routes ==================
        Route::post('update-profile', 'ClientController@update');
        Route::any('all-favorites', 'ClientController@Favorite'); // Client Favorites
        Route::any('toggle-favorites', 'ClientController@ToggleFavorite'); // Client ToggleFavorite
        Route::any('is-fav', 'ClientController@statusOfFav');


        // ================================Client notification Routes ==================
        Route::get('my_notifications', 'ClientController@myNotifications');


        // ======================= Bag Route ==============================
        Route::any('add-to-bag', 'BagController@AddToBag');             //   Add to bag
        Route::any('update-bag', 'BagController@update');               //    Update  bag
        Route::any('delete-bag', 'BagController@destroy');              //    Delete  bag
        Route::any('get-bag', 'BagController@Bag');                     //    Get  bag all in the bag


        // ================================ Address Routes ====================
        Route::any('all-address', 'AddressController@index');
        Route::any('add-address', 'AddressController@store');
        Route::post('delete-address', 'AddressController@deleteAddress');
        Route::post('update-address', 'AddressController@updateAddress');
        // Route::any('/address-show', 'AddressController@show');


        //============================== Order Routes =========================
        Route::any('current-orders', 'OrderController@current');
        Route::any('finish-orders', 'OrderController@finish');
        Route::any('store-order', 'OrderController@store');             //send request
        Route::any('order-details', 'OrderController@orderDetails');
        Route::any('delete-order', 'OrderController@delete');
        Route::any('evaluate-order', 'OrderController@evaluate');



    });

    // ===================Settings ================================
    Route::any('settings/{type}', 'SettingController@settings');


    // ================================ Provider Routes ====================
    Route::any('provider-products', 'ProviderProductController@index');  //Provider Products
    Route::any('provider-filter', 'ProviderProductController@filter');


    // ================================ City Routes ====================
    Route::any('/cities', 'CityController@index');


    // ========================== Filter Page ================================
    Route::any('filter-page', 'FilterController@Filter');
    Route::any('all-companies', 'FilterController@company');
    Route::any('all-models', 'FilterController@model');
    Route::any('all-serials', 'FilterController@serial');
    Route::any('all-production_years', 'FilterController@production_year');
    Route::any('range-price', 'FilterController@price');


    // ================================ Category Routes ====================
//    Route::any('/category', 'CategoryController@index');
//    Route::any('/category/show', 'CategoryController@show');

    // ==============TransAction Route =======================
    // Route::any('transaction', 'TransactionController@index');
    //Route::any('transaction-show', 'TransactionController@show');


});
