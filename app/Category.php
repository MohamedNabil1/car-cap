<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function getImagePathAttribute()
    {
        $image = Category::where('id', $this->id)->first()->image;
        if (!$image) {
            return asset('public/uploads/default.png');
        }
        return asset('public/uploads/categories/' . $this->image);
    }

    public function subCategory()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
