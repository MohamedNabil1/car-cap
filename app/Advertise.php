<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertise extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getImagePathAttribute()
    {
        $image = Advertise::where('id', $this->id)->first()->image;

        if (!$image) {
            return asset('public/uploads/default.png');
        } else {
            return asset('public/uploads/advertise/' . $this->image);
        }
    }

 

}
