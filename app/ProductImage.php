<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{

    protected $guarded = [];


    public function getImagePathAttribute()
    {

        $image = $this->image;
        if (!$image) {
            return asset('public/uploads/default.png');

        }
        return asset('public/uploads/products/' . $image);

    }
     public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
