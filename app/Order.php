<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }
    public function provider()
    {
        return $this->belongsTo(User::class, 'provider_id');
    }

//    public function products()
//    {
//        return $this->belongsToMany(Product::class, 'product_order', 'order_id', 'product_id');
//    }


    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    public function bag()
    {
        return $this->hasMany(Bag::class);
    }
    public function OrderProducts()
    {
        return $this->hasMany(OrderProduct::class);
    }
}
