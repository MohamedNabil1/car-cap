<?php

namespace App;

use App\Http\Controllers\ImageController;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable implements jWTSubject

{
    //protected $appends = [ 'image' ];

    use LaratrustUserTrait;
    use Notifiable;


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



//    public function setImageAttribute(Request $request,$image)
//    {
//        if($request->hasfile('image'))
//        {
//            $request ['image'] = ImageController::upload_single($image, 'uploads/users');
//        }
//    }



    public function getImagePathAttribute()
    {
        $image = User::where('id', $this->id)->first()->image;
        if (!$image) {
            return asset('public/uploads/default.png');
        }
        return asset('public/uploads/users/' . $this->image);
    }




    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function address()
    {
        return $this->hasMany(Address::class);
    }

    public function getFullNameAtrribute($value)
    {
        return ucwords($value);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function favorite()
    {
        return $this->hasMany(Favorite::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }

    public function ClientOrders()
    {
        return $this->hasMany(Order::class, 'client_id');
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
    public function ProviderOrders()
    {
        return $this->hasMany(Order::class, 'provider_id');
    }

    public function token()
    {
        return $this->hasOne(Token::class);
    }
}
