<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductRate extends Model
{

    protected $guarded = [];

    public function client()
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function provider()
    {
        return $this->belongsTo(User::class, 'provider_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
