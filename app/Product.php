<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }


    public function provider()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function getImagePathAttribute()
    {
        $image = $this->image;
        if (!$image) {
            return asset('public/uploads/default.png');
        }
        return asset('public/uploads/products/' . $image);

    }

    public function subcategory()
    {
        return $this->belongsTo(SubCategory::class, 'subcategory_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'product_order', 'product_id', 'order_id');
    }

    public function productorder()
    {
        return $this->hasMany(OrderProduct::class);
    }
    public function productRates()
    {
        return $this->hasMany(ProductRate::class);
    }


    public function productimages()
    {
        return $this->hasMany(ProductImage::class);
    }
}
