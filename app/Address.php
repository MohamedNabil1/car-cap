<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //protected $fillable = ['address', 'mobile', 'user_id', 'city_id', 'latitude', 'longitude', 'type_of_address', 'depart_number', 'area', 'nearest_figuer', 'description_ar', 'description_en'];
    protected $guarded = [];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }
}
