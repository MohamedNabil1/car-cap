<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;


class DashboardCheckPerms
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permissions = auth()->user()->permissions;
        $routes = $request->route()->getName();
//dd($permissions);

        if ($permissions == "") {
            abort(401);
        }

        if ( in_array($routes,json_decode($permissions ))) {
            return $next($request);
        }
        abort(401);

    }
}
