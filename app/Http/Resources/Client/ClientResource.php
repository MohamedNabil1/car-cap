<?php

namespace App\Http\Resources\Client;

use App\Http\Resources\AddressResource;
use App\Http\Resources\api\FavouritProductsResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = 'name_' . app()->getLocale();
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
           // 'user_rate' => $this->rates,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'image' => $this->ImagePath,
            'city_id' => $this->city->id ?? '',
            'city_name' => $this->city->$name ?? '',
            // 'address' => $this->address ? $this->address : "", //collection
            'address' => AddressResource::collection( $this->address),
            'Favourite Products' =>FavouritProductsResource::collection($this->favorite)



           // 'area' => $this->area ? $this->area : '',
        ];
    }
}
