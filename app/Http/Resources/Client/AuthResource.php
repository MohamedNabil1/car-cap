<?php

namespace App\Http\Resources\Client;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = 'name_' . app()->getLocale();
        return [
            'id' => $this->id,
            'code' => $this->code,
            'full_name' => $this->full_name,
            'type' => $this->type,
            'user_rate' => $this->rates,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'gender' => $this->gender,
            // 'address' => $this->address ? $this->address : "", //collection
            'image' => $this->ImagePath,
            'active' => $this->active,
            'city' => $this->city->$name ?? '',
            'city_id' => $this->city->id ?? '',
            'area' => $this->area ? $this->area : '',
            'jwt' => $this->token->jwt ?? '',
        ];
    }
}
