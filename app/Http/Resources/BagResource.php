<?php

namespace App\Http\Resources;

use App\Http\Resources\api\ProductImagesResource;
use Illuminate\Http\Resources\Json\JsonResource;

class BagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = 'name_' . app()->getLocale();
        $description = 'product_description_' . app()->getLocale();
        return [
            'client_id' => $this->client_id,
            'product_id' => $this->product_id,
            'product_name' => $this->products->$name,
            'company_name' => $this->products->company_name,
            'stock_count' => (int) $this->products->stock_count,

         ////  'product_description' => $this->products->$description,
            'product_price' => $this->product_price,
            'product_total_price' => $this->product_total_price,
           //// 'product_image' => $this->product_image,

            'product_image' =>$this->products->productimages->first()->ImagePath,
            'product_category_name' => $this->products->category->$name,

           //// 'product_subcategory_name' => $this->products->subcategory->$name,


            'provider_id' => $this->products->user_id,
            'tax_price' => $this->product_tax_price,
            'quantity' => $this->quantity,
        ];
    }
}
