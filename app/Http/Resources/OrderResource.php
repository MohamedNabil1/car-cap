<?php

namespace App\Http\Resources;

use App\Http\Resources\api\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'order_number' => $this['order_number'],
            'quantity' => $this['order']['total_quantity'],
            'order_price' => $this['order']['total_order_price'],
            'order_status' => $this['order']['status'],
            //'address' => ($this['address']->address),
          //  'transaction' => new TransactionResource($this['transaction']),
            //'products_details' =>  ProductDetailsResource::collection($this['products']),
        ];
    }
}
