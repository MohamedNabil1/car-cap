<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //dd('ok');
        return [
            'user_name' => $this->user->full_name,
            'cart_number' => $this->cart_number,
            'secret_code' => $this->secret_code,
            'expire_date' => $this->expire_date,
        ];
    }
}
