<?php

namespace App\Http\Resources;

use App\Http\Resources\Client\AuthResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $name = app()->getLocale();
        $description = app()->getLocale();

//dd($this->OrderProducts);

        return [
            'order_id' => $this->id,
            'order_number' => $this->order_number,
            'status' => $this->status,
            'date' => $this->created_at->toDateString() ,
            'provider_name' => $this->provider->full_name,

        ];





//        return [
//            'id' => $this->id,
//            'status' => $this->status,
//            'delivery' => (int) $this->delivery_price,
//            'tax' => (int) $this->tax_price,
//            'total_price' => (int) $this->total_order_price,
//            'type' => $this->type,
//            //'payment_type' => $this->transaction->payment_type,
//            'client_data' => new AuthResource($this->client),
//            'products' => $this->OrderProducts->transform(function ($query) use ($name, $description) {
//                return [
//                    'product_name' => $query->$name,
//                    'product_price' => $query->price,
//                    'product_description' => $query->$description,
////                    'product_category' => $query->category->$name,
////                    'product_subcategory' => $query->subcategory->$name,
////                    'product_quantity' => $query->productorder->quantity,
////                    'product_image' => $query->ImagePath,
//                ];
//            })
//
//        ];
    }
}
