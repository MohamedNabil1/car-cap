<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProviderProduct extends JsonResource
{




    public function toArray($request)
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();

        return [
            //'products' => ProductResource::collection($this['$products']),

            //'id' => $provider->products->id,
//            'image' => $this->ImagePath,
//            'name' => $this->$name,
//           // 'sub_category_name' => $this->category->subCategory->$name,
//            'price' => $this->price,


            'id' => (int) $this->id,
            'product_name' => $this->$name,
            'company_name' => $this->company_name,
            'category_name' => $this->category->$name,
            // 'category_id' => $this->category->id,
            'price' => $this->price,
           // 'avg_rate' => $this->rate_avg,
            'product_image' =>$this->productimages->first()->ImagePath,
        ];



    }
}
