<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\JsonResource;
use JWTAuth;

class ProductFilterResource extends JsonResource
{

    public function toArray($request)
    {


        $name = 'name_' . app()->getLocale();

        $description = 'description_' . app()->getLocale();

        return [

           // 'user_data' => new UserResource($this->user),
            'id' => (int) $this->id,
            'product_name' => $this->$name,
            'company_name' => $this->company_name,
            'category_name' => $this->category->$name,
           // 'category_id' => $this->category->id,
            'price' => $this->price,
            'product_image' =>$this->productimages->first()->ImagePath,


           // 'products' => new ProductResource($this->products),
           // $data['data'] = ProductFilterResource::collection($products);

        ];
    }
}
