<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CompanyResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

        //dd('kk');
//            return [
//                'company_name' => $this->company_name,
//
//            ];

        public function toArray($request)
    {
        return $this->collection->transform(function ($query)  {
            return [
                'id' => $query->id,
                'company_name' => $query->company_name,
            ];
        });
    }




}
