<?php

namespace App\Http\Resources\api;

use App\Http\Resources\CategoryCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class HomeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'products' => ProductResource::collection($this['products']),
        ];
    }
}
