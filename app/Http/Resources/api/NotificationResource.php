<?php

namespace App\Http\Resources\api;


use Illuminate\Http\Resources\Json\JsonResource;


class NotificationResource extends JsonResource
{



    public function toArray($request)
    {
        $value = 'value_' . app()->getLocale();
        return [
            'id' => $this->id,
            'type' => $this->type,
            'user_id' => $this->user_id,
            'title' => $this->title,
            'notify' => $this->$value,
            'created_at' => $this->created_at->diffForHumans(),
        ];

    }



}
