<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductionYearResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */



        public function toArray($request)
    {
        return $this->collection->transform(function ($query)  {
            return [
                'id' => $query->id,
                'production_year' => $query->production_year,
            ];
        });
    }




}
