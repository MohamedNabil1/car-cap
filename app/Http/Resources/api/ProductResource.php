<?php

namespace App\Http\Resources\api;

use App\Http\Resources\Client\AuthResource;
use Illuminate\Http\Resources\Json\JsonResource;
use JWTAuth;

class ProductResource extends JsonResource
{

    public function toArray($request)
    {


        $name = 'name_' . app()->getLocale();

        $description = 'description_' . app()->getLocale();

        return [
            'id' => (int) $this->id,
            'product_name' => $this->$name,
            'company_name' => $this->company_name,
            'category_name' => $this->category->$name,
            'category_id' => $this->category->id,
            'stock_count' => $this->stock_count,

            'description' => $this->$description,
            'price' => $this->price,
            'product_image' =>$this->productimages->first()->ImagePath,

            //'image' => $this->ImagePath,
          //  'product_images' => ProductImagesResource::collection($this->productimages),

        ];
    }
}
