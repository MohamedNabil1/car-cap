<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SerialResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */



        public function toArray($request)
    {
        return $this->collection->transform(function ($query)  {
            return [
                'id' => $query->id,
                'serial' => $query->serial,
            ];
        });
    }




}
