<?php

namespace App\Http\Resources\api;

use App\Http\Resources\Client\AuthResource;
use Illuminate\Http\Resources\Json\JsonResource;
use JWTAuth;

class FavouritProductsResource extends JsonResource
{

    public function toArray($request)
    {


        $name = 'name_' . app()->getLocale();

        $description = 'description_' . app()->getLocale();

        return [
            'id' => (int) $this->products->id,
            'product_name' => $this->products->$name,
            'company_name' => $this->products->company_name,
            'category_name' => $this->products->category->$name,
            'category_id' => $this->products->category->id,
            'stock_count' => $this->products->stock_count,
            'description' => $this->products->$description,
            'price' => $this->products->price,
            'product_image' =>$this->products->productimages->first()->ImagePath,
        ];
    }
}
