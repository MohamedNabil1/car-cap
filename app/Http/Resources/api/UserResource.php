<?php

namespace App\Http\Resources\api;

use App\Http\Resources\AddressResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */



    public function toArray($request)
    {
        $description = 'description_' . app()->getLocale();

        $name = 'name_' . app()->getLocale();
        return [
            'id' => $this->id,
            'full_name' => $this->full_name,
            'image' => $this->ImagePath,
           // 'address' => $this->address->first()->$description,
            'mobile' => $this->mobile,


            //  'address' => AddressResource::collection( $this->address),

        ];
    }
}
