<?php

namespace App\Http\Resources\api;

use App\Http\Resources\api\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderStoreResource extends JsonResource
{

    public function toArray($request)
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();


        return [



           // 'products' => $this->OrderProducts->transform(function ($query) use ( $name,$description) {
               // return [
                    'product_id' => $this->product->id,
                    'product_name' => $this->product->$name,
                    'product_price' => $this->product_price,
                    'product_description' => $this->product->$description,
                    'company_name' => $this->product->company_name,
                    'product_quantity' => $this->quantity,
                    'product_image' => $this->product->productimages->first()->ImagePath,
              //  ];
          //  }),

        ];
    }
}
