<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserAddressCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $description = 'description_' . app()->getLocale();
        $name = 'name_' . app()->getLocale();
        dd('ok');
        return [
            'id' => $this->id,
            'address' => $this->address,
            'user_id' => $this->user_id,
            'city_name' => $this->city->$name,
            'city_id' => $this->city->id,
            'latitude' => $this->latitude ?? '',
            'longitude' => $this->longitude ?? '',
            'type_of_address' => $this->type_of_address,
            'depart_number' => $this->depart_number,
            'area' => $this->area ?? '',
            'nearest_figuer' => $this->nearest_figuer  ?? '',
            'description' => $this->$description ?? '',
        ];
    }
}
