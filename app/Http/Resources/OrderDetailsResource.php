<?php

namespace App\Http\Resources;

use App\Http\Resources\api\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsResource extends JsonResource
{

    public function toArray($request)
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();


        return [

            'client_data' => new UserResource($this->provider),
            'order_id' => $this->id,
            'status' => $this->status,
            //'payment_type' => $this->transaction->payment_type,

            'products' => $this->OrderProducts->transform(function ($query) use ( $name,$description) {

                return [
                    'product_id' => $query->product->id,
                    'product_name' => $query->product->$name,
                    'product_price' => $query->product_price,
                    'product_description' => $query->product->$description,
                    'company_name' => $query->product->company_name,

                    //'product_category' => $query->category->$name,
                    'product_quantity' => $query->quantity,
                    'product_image' => $query->product->productimages->first()->ImagePath,
                ];
            }),

            'delivery' => (int)$this->delivery_price,
            'tax' => (int)$this->tax_price,
            'total_price' => (int)$this->total_order_price,
            // 'type' => $this->type,
        ];
    }
}
