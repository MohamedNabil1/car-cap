<?php

namespace App\Http\Resources;

use App\Http\Resources\api\ProductImagesResource;
use App\Http\Resources\Client\AuthResource;
use Illuminate\Http\Resources\Json\JsonResource;
use JWTAuth;

class ProductDetailsResource extends JsonResource
{

    public function toArray($request)
    {


        $name = 'name_' . app()->getLocale();

        $description = 'description_' . app()->getLocale();
        // $user = $request->user();
        // $user = null;
        // if ($request->header('Authorization') && JWTAuth::parseToken()) {
        //     $user = JWTAuth::parseToken()->toUser();
        // }
        // dd('ok');

        //dd($this->productimages);
        return [
            'id' => (int) $this->id,
            'product_name' => $this->$name,
            'company_name' => $this->company_name,
            'category_name' => $this->category->$name,
            'category_id' => $this->category->id,
            'stock_count' => (int) $this->stock_count,

//            'sub_category_name' => $this->subcategory->$name,
//            'sub_category_id' => $this->subcategory->id,
            'description' => $this->$description,
            'price' => $this->price,
            //'image' => $this->ImagePath,
            'product_images' => ProductImagesResource::collection($this->productimages),


            'user_data' => new AuthResource($this->provider),
        ];
    }
}
