<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use JWTAuth;

class SingleUser extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = null;
        if ($request->header('Authorization') && JWTAuth::parseToken()) {
            $user = JWTAuth::parseToken()->toUser();
        }
        return [
            'full_name' => $this->full_name,
            //'address' => $this->address->address,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'image' => $this->ImagePath,
            //'provider Products' =>ProductsResource::collection($this->products)

        ];
    }
}
