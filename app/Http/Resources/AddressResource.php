<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{

    public function toArray($request)
    {

        $description = 'description_' . app()->getLocale();
        $name = 'name_' . app()->getLocale();

        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'address' => $this->address,
            'latitude' => $this->latitude ?? '',
            'longitude' => $this->longitude ?? '',
            'type_of_address' => $this->type_of_address,
            'depart_number' => $this->depart_number,
            'area' => $this->area ?? '',
            'city_name' => $this->city->$name ?? '',
            'city_id' => $this->city->id ?? '',
            'nearest_figuer' => $this->nearest_figuer ?? '',
           // 'description' => $this->$description ?? '',
            'description_ar' => $this->description_ar ,
            'description_en' => $this->description_en ,
            'mobile' => $this->mobile,
        ];
    }
}
