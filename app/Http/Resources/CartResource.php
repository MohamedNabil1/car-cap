<?php

namespace App\Http\Resources;

use App\Http\Resources\Client\AuthResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {



        return [
            'tax_price' =>  $this['tax_price'],
            'delivery_price' => $this['delivery_price'],
            'total_bag_price' => $this['order_price'],
            // 'user_data' => new AuthResource(request()->user()),
            // 'transaction_options' => TransactionResource::collection($this['transactions']),
          //  'client_address' => AddressResource::collection($this['address']),
            'products_data' => BagResource::collection($this['products_data']),
        ];
    }
}
