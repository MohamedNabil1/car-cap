<?php

namespace App\Http\Controllers\Dashboard;

use App\Advertise;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\dashboard\AdvertiseRequest;
use Illuminate\Http\Request;

class AdvertisesController extends Controller
{

    public function index()
    {
        return view('dashboard.advertises.index', [
            'advertises' => Advertise::all(),
        ]);
    }


    public function create()
    {
        return view('dashboard.advertises.create', [
            'advertises' => Advertise::orderBy('created_at', 'desc')->get(),
        ]);
    }


    public function store(AdvertiseRequest $request)
    {
        $request_data = $request->except(['add', 'addNew','image']);
        if ($request->image) {

            $request_data['image'] = ImageController::upload_single($request->image, 'uploads/advertise/');
        }

        $request_data['user_id'] = auth()->user()->id;

        Advertise::create($request_data);

        if ($request->add) {
            $back =  route('advertises.index');
        } else {
            $back = route('advertises.create');
        }
        return redirect($back)->with('success', trans('api.added_successfully'));
    }


    public function show(Advertise $advertise)
    {
        //
    }


    public function edit(Advertise $advertise)
    {
        //
    }


    public function update(Request $request, Advertise $advertise)
    {
        //
    }


    public function destroy(Advertise $advertise)
    {
            $advertise->delete();
            return redirect()->route('advertises.index')->with('success', trans('api.deleted_successfully'));
    }

}
