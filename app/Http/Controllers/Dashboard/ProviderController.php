<?php

namespace App\Http\Controllers\Dashboard;

use App\Address;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\dashboard\ProviderRequest;
use App\Http\Requests\UserRequest;
use App\Order;
use App\Product;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use JWTAuth;

class ProviderController extends Controller
{

    public function __construct()
    {
        $this->mainRedirect = 'dashboard.providers.';
    }

    public function index(Request $request)
    {
        $providers = User::where('type','provider')->when($request->search, function ($query) use ($request) {
            return $query->where('full_name', 'like', '%' . $request->search . '%');
        })->latest()->paginate(4);

        return view($this->mainRedirect . 'index', [
            'providers' => $providers,
        ]);
    }


    public function create()
    {
        return view($this->mainRedirect . 'create', [
            'cities' => City::all(),
        ]);
    }


    public function store(ProviderRequest $request)
    {

        $request_data = $request->except(['password', 'password_confirmation', 'image', 'address', 'type_of_address', 'depart_number', 'nearest_figuer', 'description_ar', 'description_en', 'area', 'permissions']);
        $request_data['password'] = bcrypt($request->password);
        if ($request->image) {
            $request_data['image'] =  ImageController::upload_single($request->image, 'uploads/users/');
        }
        $request_data['code'] = rand(0000, 9999);
        $request_data['type'] = 'provider';
        $provider = User::create($request_data );

        $provider->attachRole('provider');
        if ($request->permissions) {
            $provider->syncPermissions($request->permissions);
        }

        Token::create([
            'user_id' => $provider->id,
            'jwt' => JWTAuth::fromUser($provider),
        ]);

        Address::create([
            'user_id' => $provider->id,
            'address' => $request->address,
            'mobile' => $request->mobile,
            'type_of_address' => $request->type_of_address,
            'depart_number' => $request->depart_number,
            'nearest_figuer' => $request->nearest_figuer,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'area' => $request->area,
        ]);

        return redirect()->route('providers.index')->with('success', 'تم اضافه مزود الخدمة بنجاح');
    }


    public function show(User $provider)
    {

        $name  = 'name_' . app()->getLocale();
        $description  = 'description_' . app()->getLocale();

        $order = Order::all();

        $products= Product::where('user_id', $provider->id)->paginate(1);

        $provider_pending_orders = $order->where('status','pending')->where('provider_id',$provider->id)->all();   // waiting accept تم استلام الطلب
        $on_processing_orders = $order->where('status','on_processing')->where('provider_id',$provider->id)->all();   // On Processing Orders قيد التنفيذ

        $on_shipping_orders = $order->where('status','shipping')->where('provider_id',$provider->id)->all();   // Shipping Orders تم الشحن
        $provider_delivered__orders =$order->where('status', 'delivered')->where('provider_id',$provider->id)->all();   // Delivered Orders تم التوصيل

        return view($this->mainRedirect . 'show', [
            'provider' => $provider,
           // 'products' => $provider->products,
            'products' => $products,
            'name' => $name,
            'description' => $description,
            'addresses' => $provider->address,

            'pending_orders' => $provider_pending_orders,
            'on_processing_orders' => $on_processing_orders,
            'on_shipping_orders' => $on_shipping_orders,
            'delivered_orders' => $provider_delivered__orders,
        ]);
    }


    public function edit(User $provider)
    {
        return view($this->mainRedirect . 'edit', [
            'provider' => $provider,
            'cities' => City::all(),
        ]);
    }


    public function update(ProviderRequest $request, User $provider)
    {
        $request_data = $request->except(['image', 'password', 'password_confirmation', 'permissions', 'address', 'type_of_address', 'depart_number', 'nearest_figuer', 'description_ar', 'description_en', 'area']);
        $request_data['password'] = bcrypt($request->password);
        if ($request->image) {
            $image = $provider->image;
            ImageController::delete_image($image, 'providers');
            $request_data['image'] =  ImageController::upload_single($request->image, 'uploads/users/');
        }
        $provider->update($request_data);
        return redirect()->route('providers.index')->with('success', 'تم التعديل  بنجاح');

    }


    public function destroy(User $provider)
    {
        $image = $provider->image;
        $path = 'providers';
        ImageController::delete_image($image, $path);
        $provider->delete();
        return redirect()->route('providers.index')->with('success', 'تم حذف المستخدم بنجاح');
    }



}
