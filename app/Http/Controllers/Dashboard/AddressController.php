<?php

namespace App\Http\Controllers\Dashboard;

use App\Address;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\dashboard\AddressRequest;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function __construct()
    {
        return $this->mainReturn = 'dashboard.address.';
    }

    public function index()
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();

        if (auth()->user()->hasRole('admin')) {
            $addresses = Address::all();
        } else {
            $addresses  = auth()->user()->address;
        }

        return view($this->mainReturn . 'index', [
            'addresses' => $addresses,
            'name' => $name,
            'description' => $description,
        ]);
    }


    public function create()
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();
        return view($this->mainReturn . 'create', [
            'addresses' => Address::all(),
            'cities' => City::all(),
            'name' => $name,
            'description' => $description,
        ]);
    }


    public function store(AddressRequest $request)
    {
        $request_data = $request->except(['add', 'addNew']);
        $request_data['user_id'] = auth()->user()->id;

        Address::create($request_data);

        if ($request->add) {
            $back =  route('address.index');
        } else {
            $back = route('address.create');
        }
        return redirect($back)->with('success', trans('api.added_successfully'));
    }


    public function show(Address $address)
    {
        //
    }


    public function edit(Address $address)
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();
        return view($this->mainReturn . 'edit', [
            'address' => $address,
            'cities' => City::all(),
            'name' => $name,
            'description' => $description,
        ]);
    }


    public function update(AddressRequest $request, Address $address)
    {
        $address->update($request->all());
        return redirect()->route('address.index')->with('success', trans('api.updated_successfully'));
    }


    public function destroy(Address $address)
    {
        if (auth()->user()->address->count() == 1) {
            $address->delete();
            return redirect()->route('address.index')->with('success', trans('api.deleted_successfully'));
        } else {
            return redirect()->route('address.index')->with('error', trans('api.cant_deleted_add_more_first'));
        }
    }
}
