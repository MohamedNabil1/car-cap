<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Notification;
use App\User;
use Illuminate\Http\Request;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;


//use FCM;

class NotificationController extends Controller
{

    public function sendNotify(Request $request)
    {
        $user=User::find($request->user_id);
       // dd($user->token->fcm );
        if (is_null($user)){
            return back()->with('message', 'هذا المستخدم غير موجود')->with('class', 'alert-danger');
        }
        if ($user->token->fcm != null ) {
            $data = [
                'type' => 'Car-Cap management',
                'title' => $request->title,
                'value_ar' => $request->value_ar,
            ];
           // $this->sendFCM($user->token->fcm, $data);
        }
        Notification::create([
            'type' => 'Car-Cap management ',
            'user_id' => $request->user_id,
            'title' => $request->title,
            'value_ar' => $request->value_ar,
        ]);
        return back()->with('success', 'تم إرسال الاشعار بنجاح');
    }

    protected function sendFCM($token, $data)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['value_ar'])
        ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->setData($data);
        //$dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$token = '';
        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        //$downstreamResponse->numberSuccess();
        //$downstreamResponse->numberFailure();
    }


}
