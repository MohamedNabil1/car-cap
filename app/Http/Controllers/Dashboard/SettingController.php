<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    protected $main_redirect  = 'dashboard.settings.';
    public function index()
    {
        return view($this->main_redirect . 'index');
    }

    public function update(Request $request)
    {

        if ($request->APP_NAME_AR) {
            $this->UpdateSetting('APP_NAME_AR', $request->APP_NAME_AR);
        }
        if ($request->APP_NAME_EN) {
            $this->UpdateSetting('APP_NAME_EN', $request->APP_NAME_EN);
        }
        if ($request->APP_DESC_AR) {
            $this->UpdateSetting('APP_DESC_AR', $request->APP_DESC_AR);
        }
        if ($request->APP_DESC_EN) {
            $this->UpdateSetting('APP_DESC_EN', $request->APP_DESC_EN);
        }
        if ($request->FACEBOOK_URL) {
            $this->UpdateSetting('FACEBOOK_URL', $request->FACEBOOK_URL);
        }
        if ($request->TWITTER_URL) {
            $this->UpdateSetting('TWITTER_URL', $request->TWITTER_URL);
        }
        if ($request->INSTAGRAM_URL) {
            $this->UpdateSetting('INSTAGRAM_URL', $request->INSTAGRAM_URL);
        }
        if ($request->SNAPCHAT_URL) {
            $this->UpdateSetting('SNAPCHAT_URL', $request->SNAPCHAT_URL);
        }
        if ($request->MOBILE) {
            $this->UpdateSetting('MOBILE', $request->MOBILE);
        }
        if ($request->FORMAL_EMAIL) {
            $this->UpdateSetting('FORMAL_EMAIL', $request->FORMAL_EMAIL);
        }
        if ($request->SMTP_HOST) {
            $this->UpdateSetting('SMTP_HOST', $request->SMTP_HOST);
        }
        if ($request->SMTP_PORT) {
            $this->UpdateSetting('SMTP_PORT', $request->SMTP_PORT);
        }
        if ($request->SMTP_EMAIL) {
            $this->UpdateSetting('SMTP_EMAIL', $request->SMTP_EMAIL);
        }
        if ($request->SMTP_PASSWORD) {
            $this->UpdateSetting('SMTP_PASSWORD', $request->SMTP_PASSWORD);
        }
        if ($request->ABOUT_AR) {
            $this->UpdateSetting('ABOUT_AR', $request->ABOUT_AR);
        }
        if ($request->ABOUT_EN) {
            $this->UpdateSetting('ABOUT_EN', $request->ABOUT_EN);
        }
        if ($request->SMS_PROVIDER_SENDER) {
            $this->UpdateSetting('SMS_PROVIDER_SENDER', $request->SMS_PROVIDER_SENDER);
        }
        if ($request->SMS_PROVIDER_MOBILE) {
            $this->UpdateSetting('SMS_PROVIDER_MOBILE', $request->SMS_PROVIDER_MOBILE);
        }
        if ($request->SMS_PROVIDER_PASSWORD) {
            $this->UpdateSetting('SMS_PROVIDER_PASSWORD', $request->SMS_PROVIDER_PASSWORD);
        }

        if ($request->PRIVACY_POLICY_AR) {
            $this->UpdateSetting('PRIVACY_POLICY_AR', $request->PRIVACY_POLICY_AR);
        }
        if ($request->PRIVACY_POLICY_EN) {
            $this->UpdateSetting('PRIVACY_POLICY_EN', $request->PRIVACY_POLICY_EN);
        }
        if ($request->TERMS_AR) {
            $this->UpdateSetting('TERMS_AR', $request->TERMS_AR);
        }
        if ($request->TERMS_EN) {
            $this->UpdateSetting('TERMS_EN', $request->TERMS_EN);
        }
        if ($request->APP_PERCENTAGE) {
            $this->UpdateSetting('APP_PERCENTAGE', $request->APP_PERCENTAGE);
        }

        return redirect()->route('settings.index')->with('success', trans('site.edit_successfully'));
    }

    public function UpdateSetting($key, $value)
    {
        Setting::where('key', $key)->update(['value' => $value]);
        return true;
    }

    public function edit()
    {
        return view($this->main_redirect . 'edit');
    }

//    public function Privacy()
//    {
//        return view($this->main_redirect . 'privacy', [
//            'privacy' => SETTING_VALUE('PRIVACY_POLICY_' . strtoupper(app()->getLocale())),
//        ]);
//    }
//    public function Terms()
//    {
//        return view($this->main_redirect . 'terms', [
//            'terms' => SETTING_VALUE('TERMS_' . strtoupper(app()->getLocale())),
//        ]);
//    }
//
//    public function About()
//    {
//        return view($this->main_redirect . 'about', [
//            'about' => SETTING_VALUE('ABOUT_' . strtoupper(app()->getLocale())),
//        ]);
//    }
}
