<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\dashboard\ProductRequest;
use App\Notification;
use App\Product;
use App\ProductImage;
use App\SubCategory;
use App\User;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->mainRetrun = 'dashboard.products.';
    }

    public function index(Request $request)
    {
        $products = Product::when($request->search, function ($query) use ($request) {
            return $query->where('name_ar', 'like', '%' . $request->search . '%')
                ->orwhere('name_en', 'like', '%' . $request->search . '%');
        })->latest()->paginate(5);

        return view($this->mainRetrun . 'index', [
            'products' =>  $products,
            'name' => 'name_' . app()->getLocale(),
            'description' => 'description_' . app()->getLocale(),
        ]);
    }


    public function create()
    {
        return view($this->mainRetrun . 'create', [
            'categories' => Category::all(),
            'providers' => User::where('type','provider')->get(),
            'cities' => City::all(),
            'name' => 'name_' . app()->getLocale(),
        ]);
    }

    public function store(ProductRequest $request)
    {
//        dd(auth()->user()->id);
        $request_data = $request->except(['images', 'add', 'addNew']);
        if (auth()->user()->hasRole('provider'))
        {
        $request_data['user_id'] = auth()->user()->id;
        }
        $request_data['serial']  = random_int(1111, 9999);

        if($request->hasfile('images'))
        {
            foreach($request->file('images') as $image)
            {
                $image = ImageController::upload_single($image,'uploads/products/');
                $data[] = ['image' => $image];
            }
        }

        $product = Product::create($request_data);
        $product_images = $product->productimages()->createMany($data);

        if ($request->add) {
            $back =  route('products.index');
        } else {
            $back = route('products.create');
        }
        return redirect($back)->with('success', trans('site.added_successfully'));
    }


    public function show(Product $product)
    {
        $product->update([
            'views_num' => ($product->views_num) + 1,
        ]);
        return view($this->mainRetrun . 'show', [
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        return view($this->mainRetrun . 'edit', [
            'product' => $product,
            'categories' => Category::all(),
            'cities' => City::all(),
        ]);
    }

    public function update(ProductRequest $request, Product $product)
    {
        $request_data = $request->except(['image']);
        if ($request->image) {
            ImageController::delete_image($product->image, 'products/');
            $request_data['image']  =  ImageController::upload_single($request->image, 'uploads/products/');
        }
        $product->update($request_data);
        return redirect()->route('products.index')->with('success', trans('site.updated_successfully'));
    }


    public function destroy(Product $product)
    {
        $image = $product->image;
        $path = 'products';
        ImageController::delete_image($image, $path);
        $product->delete();
        return redirect()->route('products.index')->with('success', trans('api.added_successfully'));
    }


}
