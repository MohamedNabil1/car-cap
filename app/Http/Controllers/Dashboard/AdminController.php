<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\dashboard\AdminRequest;
use App\Order;
use App\Product;
use App\Setting;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class AdminController extends Controller
{


    public function __construct()
    {
        $this->mainRedirect = 'dashboard.admins.';
    }

    public function index(Request $request)
    {
        $admins = User::where('type', 'admin')->when($request->search, function ($query) use ($request) {
            return $query->where('full_name', 'like', '%' . $request->search . '%');
        })->latest()->paginate(4);

        return view($this->mainRedirect . 'index', [
            'admins' => $admins,
        ]);
    }


    public function create()
    {
        return view($this->mainRedirect . 'create', [
            'cities' => City::all(),
            'users' => User::all(),
            'categories' => Category::all(),
            'products' => Product::all(),
            'settings' => Setting::all(),
            'all-orders' => Order::all(),
        ]);
    }


    public function store(AdminRequest $request)
    {
        $request_data = $request->except(['password', 'password_confirmation', 'image', 'permissions']);
        $request_data['password'] = bcrypt($request->password);
        if ($request->image) {
            $request_data['image'] = ImageController::upload_single($request->image, 'uploads/users/');
        }
        $request_data['type'] = 'admin';
        $request_data['code'] = rand(0000, 9999);
        $admin = User::create($request_data);

        $admin->attachRole('admin');
        if ($request->permissions) {
            $admin->syncPermissions($request->permissions);
        }

        Token::create([
            'user_id' => $admin->id,
            'jwt' => JWTAuth::fromUser($admin),
        ]);
        return redirect()->route('admins.index')->with('success', 'تم الاضافة  بنجاح');
    }


    public function show(User $admin)
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();

        return view($this->mainRedirect . 'show', [
            'user' => $admin,
            'name' => $name,
            'description' => $description,
            'addresses' => $admin->address
        ]);
    }


    public function edit(User $admin)
    {
        return view($this->mainRedirect . 'edit', [
            'admin' => $admin,
            'cities' => City::all(),
        ]);
    }

    public function update(AdminRequest $request, User $admin)
    {
        $request_data = $request->except(['image', 'password', 'password_confirmation', 'permissions', 'address',]);
        $request_data['password'] = bcrypt($request->password);
        if ($request->image) {
            $image = $admin->image;
            ImageController::delete_image($image, 'users');
            $request_data['image'] = ImageController::upload_single($request->image, 'uploads/users/');
        }
        $admin->update($request_data);
        return redirect()->route('admins.index')->with('success', 'تم  التعديل بنجاح');

    }


    public function destroy(User $admin)
    {
        $image = $admin->image;
        $path = 'users';
        ImageController::delete_image($image, $path);
        $admin->delete();
        return redirect()->route('admins.index')->with('success', 'تم الحذف  بنجاح');
    }

}
