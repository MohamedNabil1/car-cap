<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        $this->mainRedirect = 'dashboard.categories.';
    }

    public function index(Request $request)
    {
        $categories =  Category::when($request->search, function ($query) use ($request) {
            return $query->where('name_ar', 'like', '%' . $request->search . '%')->orwhere('name_en', 'like', '%' . $request->search . '%');
        })->get();

        return view($this->mainRedirect . 'index', [
            'categories' => $categories
        ]);
    }


    public function create()
    {
        return view($this->mainRedirect . 'create', [
            'categories' => Category::latest()->take(10)->get(),
        ]);
    }


    public function store(CategoryRequest $request)
    {
        $request_data = $request->except(['add', 'addNew', 'image', 'number_inner_products']);
        if ($request->image) {

            $request_data['image'] = ImageController::upload_single($request->image, 'uploads/categories/');
        }
        Category::create($request_data);
        if ($request->add) {
            $back =  route('categories.index');
        } else {
            $back = route('categories.create');
        }
        return redirect($back)->with('success', 'تم اضافه القسم بنجاح');
    }

    public function show(Category $category)
    {
        //
    }


    public function edit(Category $category)
    {
        return view($this->mainRedirect . 'edit', [
            'category' => $category,
        ]);
    }


    public function update(CategoryRequest $request, Category $category)
    {
        $request_data = $request->except('image');
        if ($request->image) {
            ImageController::delete_image($category->image, 'categories');
            $request_data['image'] = ImageController::upload_single($request->image, 'uploads/categories/');
        }
        $category->update($request_data);
        return redirect()->route('categories.index')->with('success', 'تم تعديل القسم بنجاح');
    }


    public function destroy(Category $category)
    {
        ImageController::delete_image($category->image, 'categories');
        $category->delete();
        return redirect()->route('categories.index')->with('success', trans('site.deleted_successfully'));
    }
}
