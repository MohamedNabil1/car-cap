<?php

namespace App\Http\Controllers\Dashboard;

use App\Address;
use App\City;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Requests\UserRequest;
use App\Order;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use Intervention\Image\ImageManagerStatic as Image;
use JWTAuth;

class UserController extends Controller
{

    public function __construct()
    {
        $this->mainRedirect = 'dashboard.users.';
    }

    public function index(Request $request)
    {
        $users = User::where('type','client')->when($request->search, function ($query) use ($request) {
            return $query->where('full_name', 'like', '%' . $request->search . '%');
        })->latest()->paginate(4);
        return view($this->mainRedirect . 'index', ['users' => $users,]);
    }


    public function create()
    {
        return view($this->mainRedirect . 'create', [
            'cities' => City::all(),
        ]);
    }


    public function store(UserRequest $request)
    {
        $request_data = $request->except(['password', 'password_confirmation', 'image', 'address', 'type_of_address', 'depart_number', 'nearest_figuer', 'description_ar', 'description_en', 'area', 'permissions']);
        $request_data['password'] = bcrypt($request->password);
        if ($request->image) {
            $request_data['image'] =  ImageController::upload_single($request->image, 'uploads/users/');
        }
        $request_data['type'] ='client';
        $request_data['code'] = rand(0000, 9999);
        $user = User::create($request_data);


        Token::create([
            'user_id' => $user->id,
            'jwt' => JWTAuth::fromUser($user),
        ]);

        Address::create([
            'user_id' => $user->id,
            'address' => $request->address,
            'mobile' => $request->mobile,
            'type_of_address' => $request->type_of_address,
            'depart_number' => $request->depart_number,
            'nearest_figuer' => $request->nearest_figuer,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'area' => $request->area,
            'city_id' => $request->city_id,

        ]);

        return redirect()->route('users.index')->with('success', 'تم اضافه المستخدم بنجاح');
    }


    public function show(User $user)
    {

        $name  = 'name_' . app()->getLocale();
        $description  = 'description_' . app()->getLocale();
        $user_orders = Order::where('client_id', $user->id)->get()->count();

        return view($this->mainRedirect . 'show', [
            'user' => $user,
            'products' => $user->products,
            'name' => $name,
            'description' => $description,
            'user_orders' => $user_orders,
            'addresses' => $user->address
        ]);
    }


    public function edit(User $user)
    {
        return view($this->mainRedirect . 'edit', [
            'user' => $user,
            'cities' => City::all(),
        ]);
    }


    public function update(UserRequest $request, User $user)
    {
        $request_data = $request->except(['image', 'password', 'password_confirmation', 'permissions', 'address', 'type_of_address', 'depart_number', 'nearest_figuer', 'description_ar', 'description_en', 'area']);
        $request_data['password'] = bcrypt($request->password);
        if ($request->image) {
            $image = $user->image;
            ImageController::delete_image($image, 'users');
            $request_data['image'] =  ImageController::upload_single($request->image, 'uploads/users/');
        }
        $user->update($request_data);
        return redirect()->route('users.index')->with('success', 'تم اضافه التعديل بنجاح');

    }


    public function destroy(User $user)
    {
        $image = $user->image;
        $path = 'users';
        ImageController::delete_image($image, $path);
        $user->delete();
        return redirect()->route('users.index')->with('success', 'تم حذف المستخدم بنجاح');
    }
}
