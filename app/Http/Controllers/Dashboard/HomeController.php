<?php

namespace App\Http\Controllers\Dashboard;

use App\Advertise;
use App\Category;
use App\City;
use App\Http\Controllers\Controller;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $name = 'name_' . app()->getLocale();
        $description = 'description_' . app()->getLocale();
        $users = User::where('type', 'client');
        $providers = User::where('type', 'provider');
        $all_products = Product::all();
        $latest_products = Product::latest()->paginate(3);
        $most_products = Product::orderBy('ordered_count', 'asc')->latest()->paginate(3);
        $orders = Order::all();
        $advertises = Advertise::all();
        $cities = City::all();
        $categories = Category::all();

        return view('dashboard.index', [
            'users' => $users,
            'providers' => $providers,
            'all_products' => $all_products,
            'latest_products' => $latest_products,
            'most_products' => $most_products,
            'orders' => $orders,
            'advertises' => $advertises,
            'cities' => $cities,
            'description' => $description,
            'name' => $name,
            'categories' => $categories,
        ]);

    }
}
