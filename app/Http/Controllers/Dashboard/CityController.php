<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\City;
use App\Http\Requests\CityRequest;
use Illuminate\Http\Request;

class CityController extends Controller
{

    public function index()
    {
        return view('dashboard.city.index', [
            'cities' => City::all(),
        ]);
    }

    public function create()
    {
        return view('dashboard.city.create', [
            'cities' => City::all(),
        ]);
    }


    public function store(CityRequest $request)
    {
        $request_data = $request->except(['add', 'addNew']);
        City::create($request_data);
        if ($request->add) {
            $back =  route('city.index');
        } else {
            $back = route('city.create');
        }
        return redirect($back)->with('success', 'تم اضافه المدينه بنجاح');
    }

    public function show(City $city)
    {
        //
    }

    public function edit(City $city)
    {
        return view('dashboard.city.edit', [
            'city' => $city
        ]);
    }

    public function update(CityRequest $request, City $city)
    {
        $city->update([
            'name_ar' => $request->name_ar,
            'name_en' => $request->name_en,
        ]);
        return redirect()->route('city.index')->with('success', 'تم تعديل المدينه بنجاح');
    }

    public function destroy(City $city)
    {
        $city->delete();
        return redirect()->route('city.index')->with('success', 'تم حذف المدينه بنجاح');
    }
}
