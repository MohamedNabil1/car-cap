<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Notification;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function __construct()
    {
        $this->main_redirect = 'dashboard.orders.';
    }

    public function index()
    {

       // $order = Order::where('provider_id', auth()->user()->id)->get();
        $products = Product::all();
        $order = Order::all();
        $name = 'name_' . app()->getLocale();


        $provider_pending_orders = $order->where('status', 'pending')->all();        // waiting accept تم استلام الطلب
        $on_processing_orders = $order->where('status', 'on_processing')->all();     // On Processing Orders قيد التنفيذ
        $on_shipping_orders = $order->where('status', 'shipping')->all();            // Shipping Orders تم الشحن
        $provider_delivered__orders = $order->where('status', 'delivered')->all();   // Delivered Orders تم التوصيل


        return view($this->main_redirect . 'index', [
            'products' => $products,
            'order' => $order,
            'pending_orders' => $provider_pending_orders,
            'on_processing_orders' => $on_processing_orders,
            'on_shipping_orders' => $on_shipping_orders,
            'delivered_orders' => $provider_delivered__orders,
            'name' => $name,
        ]);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show(Order $order)
    {
        $name = 'name_' . app()->getLocale();
        return view($this->main_redirect . 'show', [
            'order_products' => $order->OrderProducts,
            'name' => $name
        ]);
    }


    public function edit(Order $order)
    {
        //
    }


    public function update(Request $request, Order $order)
    {
        //dd($order);
    }



    public function process(Order $order)
    {
        $order->update(['status' => 'on_processing']);
       // return redirect()->route('orders.index');
        return back();
    }


    public function ship(Order $order)
    {
        $order->update(['status' => 'shipping']);
       // return redirect()->route('orders.index');
        return back();

    }


    public function deliver( Request $request ,Order $order)

    {
        $order->update(['status' => 'delivered']);

//        $user=User::where('id',$order->client_id)->first();
//       // dd($user->id);
//        if (is_null($user)){
//            return back()->with('message', 'هذا المستخدم غير موجود')->with('class', 'alert-danger');
//        }

//        if ($user->fcm != null ) {
//            $data = [
//                'type' => 'Car-Cap management',
//                'title' => 'notification',
//                'value_ar' => 'your order delivered',
//            ];
//            $this->sendFCM($user->fcm, $data);
//        }

//        Notification::create([
//            'type' => 'Car-Cap management',
//            'user_id' => $user->id,
//            'title' => 'notification',
//            'value_ar' => 'your order delivered',
//        ]);

       // return redirect()->route('orders.index');
        return back();

    }


    public function destroy(Order $order)
    {
        //
    }


    protected function sendFCM($token, $data)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['value_ar'])
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->setData($data);
        //$dataBuilder->addData($data);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$token = '';
        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);


    }

}
