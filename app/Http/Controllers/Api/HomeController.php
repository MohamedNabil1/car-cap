<?php

namespace App\Http\Controllers\Api;

use App\Advertise;
use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\SearchRequest;
use App\Http\Resources\api\ProductResource;
use App\Http\Resources\api\SliderResource;
use App\Http\Resources\CategoryCollection;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\HomeRequest;
use App\Http\Resources\api\HomeResource;
use App\Http\Resources\ProductDetailsResource;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends PARENT_API
{

    public function search(SearchRequest $request)
    {
        $products = Product::where('name_ar', 'like', '%' . $request->product_name . '%')->get();
        if ($products->count() == 0) {
            $this->data['data'] = [];
            $this->data['message'] = 'no products';
            $this->data['status'] = 'fails';
            return response()->json($this->data, 405);
        }

        $this->data['data'] = ProductResource::collection($products);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);

    }

    public function categories(Request $request)
    {
        $categories = Category::all();
        if ($categories->count() == 0) {
            $this->data['data'] = [];
            $this->data['message'] = 'no categories';
            $this->data['status'] = 'fails';
            return response()->json($this->data, 405);
        }

        $this->data['data'] = CategoryCollection::collection($categories);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);

    }

    public function index(HomeRequest $request)
    {
        $categories = Category::where('id', $request->category_id)->get();
        $type = $request->key;

        if (!in_array($type, ['recent', 'most_sale', 'new_parts', 'used_parts'])) {
            $this->data['data'] = null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('dash.key not found');
            return response()->json($this->data, 405);
        }

        if ($categories->count() == 0) {
            $this->data['data'] = [];
            $this->data['message'] = trans('dash.id not found');
            $this->data['status'] = 'fails';

            return response()->json($this->data, 405);
        }

        if ($request->category_id == 0) {
            switch ($type) {
                case 'recent':
                    $products = Product::orderBy('created_at', 'desc')->get();
                    break;
                case 'most_sale':
                    $products = Product::orderBy('ordered_count', 'desc')->get();
                    break;
                case 'new_parts':
                    $products = Product::where('status', 'new')->get();
                    break;
                case 'used_parts':
                    $products = Product::where('status', 'used')->get();
                    break;
            }
        } else {
            switch ($type) {
                case 'recent':
                    $products = Product::orderBy('created_at', 'desc')->where('category_id', $request->category_id)->get();
                    break;
                case 'most_sale':
                    $products = Product::orderBy('ordered_count', 'desc')->where('category_id', $request->category_id)->get();
                    break;
                case 'new_parts':
                    $products = Product::where('status', 'new')->orderBy('status', 'desc')->where('category_id', $request->category_id)->get();
                    break;
                case 'used_parts':
                    $products = Product::where('status', 'used')->orderBy('status', 'desc')->where('category_id', $request->category_id)->get();
                    break;
            }
        }

        $this->data['data'] = new HomeResource(['categories' => $categories, 'products' => $products]);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';

        return response()->json($this->data, 200);
    }


    public function slider(Request $request)
    {
        $advertisements = Advertise::all();
        $data['data'] = SliderResource::collection($advertisements);
        $data['status'] = 'ok';
        $data['message'] = '';
        return response()->json($data, 200);
    }


}
