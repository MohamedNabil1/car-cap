<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PARENT_API;
use Illuminate\Http\Request;

class SettingController extends PARENT_API
{
    public function settings($type)
    {
        if (!in_array($type, ['about', 'policy', 'terms', 'contact'])) {
            $this->data['data'] = null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('dash.not_found');
            return response()->json($this->data, 405);
        }
        switch ($type) {
            case 'about':
                $this->data['data'] = SETTING_VALUE('ABOUT_' . strtoupper(app()->getLocale()));
                break;
            case 'policy':
                $this->data['data'] = [
                    'policy' => SETTING_VALUE('PRIVACY_POLICY_' . strtoupper(app()->getLocale())),
                ];
                break;
            case 'terms':
                $this->data['data'] = SETTING_VALUE('TERMS_' . strtoupper(app()->getLocale()));
                break;
            case 'contact':
                $this->data['data'] = [
                    'FACEBOOK_URL' => SETTING_VALUE('FACEBOOK_URL'),
                    'TWITTER_URL' => SETTING_VALUE('TWITTER_URL'),
                    'INSTAGRAM_URL' => SETTING_VALUE('INSTAGRAM_URL'),

                ];
                break;
        }
        $this->data['status'] = 'ok';
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }
}
