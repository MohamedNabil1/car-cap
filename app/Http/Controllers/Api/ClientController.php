<?php

namespace App\Http\Controllers\Api;

use App\Favorite;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\ClientFavoritesRequest;
use App\Http\Requests\api\ToggleFavoriteRequest;
use App\Http\Requests\api\UpdateClientRequest;
use App\Http\Resources\api\FavouritProductsResource;
use App\Http\Resources\api\NotificationResource;
use App\Http\Resources\api\ProductResource;
use App\Http\Resources\Client\AuthResource;
use App\Http\Resources\Client\ClientResource;
use App\Http\Resources\ProductDetailsResource;
use App\Notification;
use App\User;
use Illuminate\Http\Request;

class ClientController extends PARENT_API
{

    public function update(UpdateClientRequest $request)
    {
        $client = auth()->user();
        if (!$client) {
            $this->data['data'] = '';
            $this->data['status'] = 'fails';
            $this->data['message'] = trans('api.data_not_found');
            return response()->json($this->data, 405);
        }

       // $request_data = $request->except(['image']);

//        if ($request->image) {
//            $request_data['image']  =  ImageController::upload_single($request->image, 'uploads/users/');
//        }
        $client->update($request->all());

        $this->data['data'] = new ClientResource($client);
        $this->data['status'] = 'ok';
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }


    public function Favorite()
    {
        $user = $this->user;
//        $products =  $user->favorite;
        $products = Favorite::where('user_id', $user->id)->get();

        if ($products == null) {
            $this->data['data'] = '';
            $this->data['status'] = 'fails';
            $this->data['message'] = trans('api.data_not_found');

            return response()->json($this->data, 405);
        }
        $this->data['data'] = FavouritProductsResource::collection($products);
        $this->data['status'] = 'ok';
        $this->data['message'] = '';
        return response()->json($this->data, 200);
    }

    public function ToggleFavorite(ToggleFavoriteRequest $request)
    {
        if (Favorite::where('product_id', $request->product_id)->where('user_id', $this->user->id)->exists()) {
            Favorite::where('product_id', $request->product_id)->where('user_id', $this->user->id)->delete();

            $data['data'] = '';
            $data['message'] = trans(' deleted from Favorite');
            $data['status'] = 'ok';

            return response()->json($data, 200);
        } else {
            Favorite::create([
                'user_id' => $this->user->id,
                'product_id' => $request->product_id
            ]);

            $data['data'] = '';
            $data['message'] = trans('added to Favorite');
            $data['status'] = 'ok';

            return response()->json($data, 200);
        }
    }


    public function statusOfFav(ToggleFavoriteRequest $request)
    {

        $product = Favorite::where('product_id', $request->product_id)->where('user_id', $this->user->id)->first();
        if ($product) {
            $data['data'] = 'true';
            $data['message'] = trans('fav product');
            $data['status'] = 'ok';

            return response()->json($data, 200);
        } else {
            $data['data'] = 'false';
            $data['message'] = trans('not fav product');
            $data['status'] = 'ok';
            return response()->json($data, 200);
        }
    }


    public function myNotifications(Request $request)
    {
        $user = $this->user;
        $notifications = Notification::orderBy('id', 'desc')->where('user_id', $user->id)->get();

        $this->data['data'] = NotificationResource::collection($notifications);
        $this->data['status'] = 'ok';
        $this->data['message'] = '';
        return response()->json($this->data, 200);


    }


}
