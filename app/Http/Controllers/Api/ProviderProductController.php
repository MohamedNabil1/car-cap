<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\ProviderProductRequest;
use App\Http\Resources\Client\AuthResource;
use App\Http\Resources\ProviderProduct;
use App\Http\Resources\SingleUser;
use App\Product;
use App\User;

class ProviderProductController extends PARENT_API
{
    public function index(ProviderProductRequest $request)
    {
        $provider = User::where('id', $request->provider_id)->first();
        $products = Product::where('user_id', $request->provider_id)->get();

       // $provider_products = $provider->products;

        if ($products->count() == 0) {
            $this->data['data'] = [];
            $this->data['status'] = 'fails';
            $this->data['message'] = trans('api.no_data_found');
            return response()->json($this->data, 405);
        }

        $data['data'] = [
            'user_data' => new SingleUser($provider),
            'products' => ProviderProduct::collection($products)
        ];
        $data['message'] = '';
        $data['status'] = 'ok';
        return response()->json($data, 200);
    }





    public function filter(ProviderProductRequest $request)
    {
        $products = Product::where('user_id', $request->provider_id)->get();

        if ($products->count() == 0) {
            $data['data'] = [];
            $data['message'] = trans('api.no_data_found');
            $data['status'] = 'fails';
            return response()->json($data, 405);
        }

        if ($request->less_price) {
            $products = Product::OrderBy('price', 'asc')->get();
        }
        if ($request->high_price) {
            $products = Product::OrderBy('price', 'desc')->get();
        }
        if ($request->high_order) {
            $products = Product::OrderBy('ordered_count','desc')->get();
        }
        if ($request->high_rate) {
            $products = Product::OrderBy('rate_avg', 'desc')->get();
        }


        if (!$products) {
            $data['data'] = [];
            $data['message'] = trans('api.no_data_found');
            $data['status'] = 'fails';
            return response()->json($data, 405);
        }

        $data['data'] =  ProviderProduct :: collection($products);
        $data['status'] = 'ok';
        $data['message'] = '';
        return response()->json($data, 200);
    }


}
