<?php

namespace App\Http\Controllers\Api;

use App\Bag;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\BagRequest;
use App\Http\Requests\api\UpdateBagRequest;
use App\Http\Resources\BagResource;
use App\Http\Resources\CartResource;
use App\Notification;
use App\Order;
use App\Product;
use Facade\FlareClient\Http\Response;

class BagController extends PARENT_API
{


    public function AddToBag(BagRequest $request)
    {
        $product = Product::where('id', $request->product_id)->first();
        if (is_null($product)) {
            $this->data['data'] = '';
            $this->data['status'] = 'ok';
            $this->data['message'] = trans('api.no_data_found');
            return response()->json($this->data, 405);
        }

        $bag = Bag::where('client_id', $this->user->id)->where('product_id', $request->product_id)->first();
        if (is_null($bag)) {
            $bag = Bag::create([
                'product_id' => $request->product_id,
                'product_name_ar' => $product->name_ar,
                'product_name_en' => $product->name_en,
                'product_price' => $product->price,
                'product_category_id' => $product->category->id,
                'product_total_price' => $product->price + $product->provider->delivery_price + $product->provider->tax_price,
                'client_id' => $this->user->id,
                'provider_id' => $product->user_id,
                'product_tax_price' => $product->provider->tax_price,
                'product_delivery_price' => $product->provider->delivery_price,
                'quantity' => 1
            ]);

            $this->data['data'] = '';
            $this->data['message'] = trans('api.added_successfully');
            $this->data['status'] = 'ok';
            return response()->json($this->data, 200);
        }
        else if ($bag->quantity < $product->stock_count) {

            $bag->quantity += 1;
            $bag->product_total_price = ($bag->product_price * $bag->quantity) + $bag->product_tax_price + $bag->product_delivery_price;
            $bag->update();

            $this->data['data'] = '';
            $this->data['message'] = trans('api.added_successfully');
            $this->data['status'] = 'ok';
            return response()->json($this->data, 200);
        } else {

            $this->data['data'] = '';
            $this->data['message'] = trans('sorry cant add not available quantity ');
            $this->data['status'] = 'ok';
            return response()->json($this->data, 200);
        }
    }


    // ===================== get Bag ==================
    public function Bag()
    {
        $bag_data = Bag::where('client_id', auth()->user()->id)->get();

        if (is_null($bag_data)) {
            $this->data['data'] = [];
            $this->data['status'] = "fails";
            $this->data['message'] = trans('api.no_data_found');
            return response()->json($this->data, 405);
        }

        $order_price = 0;
        $tax_price = 0;
        $delivery_price = 0;
        $total_quantity = 0;
        $total_products_price = 0;

        foreach ($bag_data as $item) {
            $tax_price += $item->product_tax_price;
            $delivery_price += $item->product_delivery_price;
            $total_quantity += $item->quantity;
            $total_products_price += $item->product_total_price;
        }
        $order_price = $tax_price + $delivery_price + $total_products_price;

        $transactions = auth()->user()->transactions;
        $address = $this->user->address;



        $this->data['data'] = new CartResource(['products_data' => $bag_data, 'order_price' => $order_price, 'tax_price' => $tax_price, 'delivery_price' => $delivery_price, 'transactions' => $transactions, 'address' => $address]);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';

        return response()->json($this->data, 200);
    }


    public function destroy()
    {
        $selected_bag = Bag::where('client_id', auth()->user()->id)->get();

        if ($selected_bag->count() == 0) {
            $this->data['data'] = [];
            $this->data['status'] = "fails";
            $this->data['message'] = trans('api.no_data_found');
            return response()->json($this->data, 405);
        }

        //  $selected_bag->order ?? $selected_bag->order->delete();

        foreach ($selected_bag as $item) {
            $item->delete();
        }

        $this->data['data'] = [];
        $this->data['status'] = "ok";
        $this->data['message'] = trans('app.deleted_successfully');
        return response()->json($this->data, 200);
    }


    public function update(UpdateBagRequest $request)
    {

        $bag = Bag::where('client_id', auth()->user()->id)->where('product_id', $request['product_id'])->first();
        if (is_null($bag)) {
            $this->data['data'] = null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('api.no_data_found');
            return response()->json($this->data, 405);
        }

        if ($request->quantity <= $bag->products->stock_count)
        {
            foreach ($bag as $item) {
                $bag->quantity = $request->quantity;
                $bag->product_total_price = $bag->product_price * $bag->quantity;
                $bag->update();
            }

            $bag_data = Bag::all();
            $bag_data = Bag::where('client_id', auth()->user()->id)->get();

            $order_price = 0;
            $tax_price = 0;
            $delivery_price = 0;
            $total_quantity = 0;
            $total_products_price = 0;

            foreach ($bag_data as $item) {
                $tax_price += $item->product_tax_price;
                $delivery_price += $item->product_delivery_price;
                $total_quantity += $item->quantity;
                $total_products_price += $item->product_total_price;
            }
            $order_price = $tax_price + $delivery_price + $total_products_price;

            // $bag = Bag::where('client_id', auth()->user()->id)->get();

            $transactions = auth()->user()->transactions;
            $address = $this->user->address;

            $this->data['data'] = new CartResource(['products_data' => $bag_data, 'order_price' => $order_price, 'tax_price' => $tax_price, 'delivery_price' => $delivery_price, 'transactions' => $transactions, 'address' => $address]);
            $this->data['status'] = 'ok';
            $this->data['message'] = trans('api.updated_successfully');
            return response()->json($this->data, 200);
        }

        $this->data['data'] = null;
        $this->data['status'] = "fails";
        $this->data['message'] = trans('not available quantity in this product stock ');
        return response()->json($this->data, 405);
    }


//    public function Complete()
//    {
//        $bag = Bag::all();
//        if (is_null($bag)) {
//            $this->data['data'] = [];
//            $this->data['status'] = "fails";
//            $this->data['message'] = trans('api.no_data_found');
//            return response()->json($this->data, 405);
//        }
//        $bag_data = $bag;
//        $order_price = 0;
//        $tax_price = 0;
//        $delivery_price = 0;
//        $total_quantity = 0;
//        $total_products_price = 0;
//        foreach ($bag_data as $item) {
//            $tax_price += $item->product_tax_price;
//            $delivery_price += $item->delivery_price;
//            $total_quantity += $item->quantity;
//            $total_products_price += $item->product_total_price;
//        }
//        $order_price = $tax_price + $delivery_price + $total_products_price;
//        // $transactions = auth()->user()->transactions;
//        $address = $this->user->address;
//        $this->data['data'] = new CartResource(['products_data' => $bag_data, 'order_price' => $order_price, 'tax_price' => $tax_price, 'delivery_price' => $delivery_price, 'transactions' => 'cache', 'address' => $address]);
//        $this->data['message'] = trans('api.updated_successfully');
//        $this->data['status'] = 'ok';
//        return response()->json($this->data, 200);
//    }





}
