<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PARENT_API;
use App\Http\Resources\CityCollection;
use Illuminate\Http\Request;

class CityController extends PARENT_API
{
    public function index()
    {
        $cities = City::all();
        $this->data['data'] = new CityCollection($cities);
        $this->data['message'] = '';
        $this->data['status'] = '';

        return response()->json($this->data, 200);
    }
}
