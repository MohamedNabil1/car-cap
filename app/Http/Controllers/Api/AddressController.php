<?php

namespace App\Http\Controllers\Api;

use App\Address;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\AddressDeleteRequest;
use App\Http\Requests\api\AddressShowRequest;
use App\Http\Requests\api\AddressStoreRequest;
use App\Http\Requests\api\AddressUpdateRequest;
use App\Http\Resources\AddressResource;
use App\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class AddressController extends PARENT_API
{
    public function index()
    {
        $user = $this->user;
        if (!$user) {
            $this->data['data'] = null;
            $this->data['message'] = trans('api.user_not_fount');
            $this->data['status'] = 'fails';
            return response()->json($this->data, 405);
        }
        $user_address = $user->address;
        $this->data['data'] = AddressResource::collection($user_address);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }


    public function show(AddressShowRequest $request)
    {

//        $address = Address::where('id', $request->address_id)->first();
//
//        if (!$address) {
//            $this->data['data'] = null;
//            $this->data['message'] = 'fails';
//            $this->data['status'] = trans('api.address_not_found');
//
//            return response()->json($this->data, 405);
//        }
//
//        $this->data['data'] = new AddressResource($address);
//        $this->data['message'] = '';
//        $this->data['status'] = 'ok';
//
//        return response()->json($this->data, 200);
    }

    public function store(AddressStoreRequest $request)
    {
        $user = $this->user;
        if (!$user) {
            $this->data['data'] = null;
            $this->data['message'] = 'fails';
            $this->data['status'] = trans('api.user_not_fount');

            return response()->json($this->data, 405);
        }

        $new_address = Address::create([
            'user_id' => $this->user->id,
            'type_of_address' => $request->type_of_address,
            'depart_number' => $request->depart_number,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            'mobile' => $request->mobile,
            'nearest_figuer' => $request->nearest_figuer ? $request->nearest_figuer : null,
        ]);

        //$this->data['data'] = new AddressResource($new_address);
        $this->data['data'] = '';
        $this->data['message'] = trans('api.added_successfully');

        $this->data['status'] = 'ok';

        return response()->json($this->data, 200);
    }


    public function updateAddress(AddressUpdateRequest $request)
    {
        $user_id = auth()->user()->id;
        $address = Address::where(['id' => $request->address_id, 'user_id' => $user_id])->first();

        if (!$address) {
            $this->data['data'] = '';
            $this->data['status'] = "fails";
            $this->data['message'] = trans('api.no_data_found');
            return response()->json($this->data, 405);
        }

        $address->update([
            'id' => $request->address_id,
            'user_id' => $request->user()->id,
            'type_of_address' => $request->type_of_address,
            'mobile' => $request->mobile,
            'depart_number' => $request->depart_number,
            'description_ar' => $request->description_ar,
            'description_en' => $request->description_en,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'address' => $request->address,
            'nearest_figuer' => $request->nearest_figuer,

        ]);
        $this->data['data'] = '';
        $this->data['status'] = "ok";
        $this->data['message'] = trans('api.updated_successfully');

        return response()->json($this->data, 200);
    }


    public function deleteAddress(AddressDeleteRequest $request)
    {
        $address = Address::find($request->address_id);
        if ($address) {
            if ($address->user_id == auth()->user()->id) {
                $address->delete();

                $this->data['data'] = '';
                $this->data['status'] = "ok";
                $this->data['message'] = trans('app.deleted_successfully');
                return response()->json($this->data, 200);
            }

            $this->data['data'] = '';
            $this->data['status'] = "fails";
            $this->data['message'] = trans('api.no_data_found');
            return response()->json($this->data, 405);
        }

        $this->data['data'] = [];
        $this->data['status'] = "fails";
        $this->data['message'] = trans('api.no_data_found');
        return response()->json($this->data, 405);

    }
}
