<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\PARENT_API;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\SmsController;
use App\Http\Requests\api\ActivateAuth;
use App\Http\Requests\api\DoResetPassword;
use App\Http\Requests\api\LoginRequest;
use App\Http\Requests\api\ResendCodeRequest;
use App\Http\Requests\api\ResetRequest;
use App\Http\Requests\api\UserRegisterRequest;
use App\Http\Resources\Client\AuthResource;
use App\Token;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class AuthController extends PARENT_API
{

    //======================= Create New User =========================

    public function Register(UserRegisterRequest $request)
    {
        $request_data = $request->except(['image', 'password', 'password_confirmation','device_type','fcm_token']);
        $request_data['password'] = bcrypt($request->password);
        $request_data['reset_code'] = random_int(1111, 9999);
        $request_data['code'] = random_int(1111, 9999);
        $request_data['type'] = 'client';
        if ($request->image) {
            $request_data['image'] =  ImageController::upload_single($request->image, 'uploads/users/');
        }

//        if ($request->latitude) {
//            $request_data['latitude'] = $request->latitude;
//        }
//        if ($request->longitude) {
//            $request_data['longitude']  = $request->longitude;
//        }

        $user = User::create($request_data);

        if ($user) {
            $token = new Token();
            $token->user_id = $user->id;
            $token->fcm = $request->fcm_token;
            $token->device_type = $request->header('os');
            $token->jwt = JWTAuth::fromUser($user);
            $token->is_logged_in = 'true';
            $token->ip = $request->ip();
            $token->save();
        }

            $data = new AuthResource($user);
            $this->data['data'] = $data;
            $this->data['message'] = "";
            $this->data['status'] = "ok";
            return response()->json($this->data, 200);


    }

    public function ActivationAuthSms(ActivateAuth $request)
    {
        $code = $request->type == 'register' ? 'code' : 'reset_code';
        $user = User::where([$code => $request->code, 'mobile' => $request->mobile])->first();
        if (!$user) {
            $this->data['data'] = "";
            $this->data['status'] = "fails";
            $this->data['message'] = trans('auth.not_found');
            return response()->json($this->data, 405);
        }

        $user->code = '';
        if ($code == 'code') {
            $user->active = 'active';
        }
        $user->update();
        $msg = trans('auth.success_login');
        if ($user->register_complete == 0) {
            $msg = trans('auth.register_completed');
        }
        $this->data['data'] = new AuthResource($user);
        $this->data['message'] = $msg;
        $this->data['type'] = $request->type;
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }


    //======================= User LogIn =========================

    public function login(LoginRequest $request)
    {
        $perms['email'] = $request->email;
        $perms['password'] = $request->password;
        if (!$token = JWTAuth::attempt($perms)) {
            $this->data['data'] = "";
            $this->data['status'] = "fails";
            $this->data['message'] = trans('auth.failed');
            return response()->json($this->data, 401);
        }

        //get logged in user token
        $logged_user = auth()->user();
        $logged_user_token = $logged_user->token;
        $logged_user_token->jwt = $token;
        $logged_user_token->is_logged_in = "true";
        //$logged_user_token->fcm = $request->fcm_token;
        $logged_user_token->device_type = $request->header('os');
        $logged_user_token->ip = $request->ip();
        $logged_user_token->update();

        if ($logged_user->active == 'active') {
            $data = new AuthResource($logged_user);
            $this->data['data'] = $data;
            $this->data['os'] = $request->header('os');
            $this->data['status'] = 'ok';
            $this->data['message'] = '';
            return response()->json($this->data, 200);
       }

        $data['mobile'] = $logged_user->mobile;
        $data['active'] = 'deactive';

        $this->data['data'] = $data;
        $this->data['status'] = 'ok';
        $this->data['message'] = '';

        return response()->json($this->data, 203);
    }


    // ========= for forget password  =======================

    public function ResetPassword(ResetRequest $request)
    {
        $mobile = $request->mobile;
        $user = User::where('mobile', $mobile)->first();

        if (!$user->exists()) {
            $this->data['data'] = null;
            $this->data['status'] = 'fails';
            $this->data['message'] = trans('auth.not_found');

            return response()->json($this->data, 403);
        }

        $reset_code = $user->reset_code;
        $reset_code_msg = trans('auth.reset_code_msg', ['code' => $reset_code], 'ar');
        (new SmsController())->send_sms($user->mobile, $reset_code_msg);
        $this->data['data'] = array();
        $this->data['data']['reset_code'] = $reset_code;
        $this->data['status'] = "ok";
        $this->data['message'] = trans('auth.reset_code_sent');
        return response()->json($this->data, 200);
    }


    // ========= for forget password =======================

    public function DoResetPassword(DoResetPassword $request)
    {
        $user = $this->user;

        $user->password = bcrypt($request->password);
        $user->reset_code = '';
        $user->update();

        $this->data['data'] =  new AuthResource($user);
        $this->data['status'] = "ok";
        $this->data['message'] = trans('auth.success_login');
        return response()->json($this->data, 200);
    }


    //================= Resend Code ===================

    public function ResendCode(ResendCodeRequest $request)
    {
        $mobile = $request->mobile;
        $user = User::where('mobile', $mobile);
        if (!$user->exists()) {
            $this->data['data'] = null;
            $this->data['status'] = "fails";
            $this->data['message'] = trans('auth.mobile_not_exist');
            return response()->json($this->data, 403);
        }
        if ($request->type == 'active') {
            $user = $user->first();
            $code = random_int(0000, 9999);
            $user->code = $code;
            $user->update();

            (new SmsController())->send_sms($user->mobile, $code);
        } else {
            $user = $user->first();
            $reset_code = random_int(0000, 9999);
            $user->reset_code = $reset_code;
            $user->update();
            (new SmsController())->send_sms($user->mobile, $user->code);
        }

        $this->data['data'] = null;
        $this->data['status'] = "ok";
        $this->data['message'] = trans('auth.reset_code_sent');
        return response()->json($this->data, 200);
    }

    //================= LogOut Controller ===================

    public function Logout()
    {
        $user_token = $this->user->token;
        $user_token->is_logged_in = 'false';
        $user_token->fcm = '';
        $user_token->update();
        $this->data['data'] = null;
        $this->data['status'] = "ok";
        $this->data['message'] = "";
        return response()->json($this->data, 200);
    }
}
