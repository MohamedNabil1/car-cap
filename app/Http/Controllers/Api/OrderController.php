<?php

namespace App\Http\Controllers\Api;

use App\Address;
use App\Bag;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\OrderDeleteRequest;
use App\Http\Requests\api\OrderEvaluateRequest;
use App\Http\Resources\api\FavouritProductsResource;
use App\Http\Resources\api\OrderStoreResource;
use App\Http\Resources\api\ProductResource;
use App\Http\Resources\OrderDetailsResource;
use App\Order;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\OrderRequest;
use App\Http\Resources\OrderProductResource;
use App\Http\Resources\OrderResource;
use App\OrderProduct;
use App\Product;
use App\ProductRate;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends PARENT_API
{

    public function current()
    {
        $current_orders = Order::where('client_id', $this->user->id)->where('status', '!=', 'delivered')->get();
        if (is_null($current_orders))
        {
            $data['data'] = [];
            $data['message'] = trans('api.data_not_found');
            $data['status'] = 'fails';
            return response()->json($data, 405);
        }
        $data['data'] = OrderProductResource::collection($current_orders);
        $data['message'] = '';
        $data['status'] = 'ok';
        return response()->json($data, 200);
    }

    public function finish()
    {
        $current_orders = Order::where('client_id', $this->user->id)->where('status', '=', 'delivered')->get();
        if (is_null($current_orders))
        {
            $data['data'] = [];
            $data['message'] = trans('api.data_not_found');
            $data['status'] = 'fails';
            return response()->json($data, 405);
        }
        $data['data'] = OrderProductResource::collection($current_orders);
        $data['message'] = '';
        $data['status'] = 'ok';
        return response()->json($data, 200);
    }


    public function store(OrderRequest  $request)
    {
       $client_id = auth()->user()->id;
        $bags = Bag::where('client_id',$client_id)->get();
        $total_price = 0;
        $total_quantity = 0;
        foreach ($bags as $bag ) {
            $provider = User::where('id', $bag->provider_id)->first();
            $total_price += $bag->product_total_price;
            $total_quantity += $bag->quantity;
        }
        $order = Order::create([
            'provider_id' => $provider->id,
            'order_number' => random_int(0000, 9999),
            'status' => 'pending',
            'address_id' => $request->address_id,
            'client_id' => $client_id,
            'delivery_price' => $provider->delivery_price,
            'tax_price' => $provider->tax_price,
            'total_quantity' => $total_quantity,
            'total_order_price' => $total_price
        ]);

        foreach ($bags as $key) {
            $order_product  = OrderProduct::create([
                'order_id' => $order->id,
                'provider_id' => $key->provider_id,
                'client_id' => $client_id,
                'product_id' => $key->product_id,
                'product_price' => $key->product_price,
                'product_tax_price' => $key->product_tax_price,
                'product_delivery_price' => $key->product_delivery_price,
                'product_total_price' => $key->product_total_price,
                'quantity' => $key->quantity
            ]);
            $key->products->stock_count=$key->products->stock_count - $key->quantity;
            $key->products->update();
            $key->delete();
        }
        $address = Address::where('id', $request->address_id)->first();
       $order = Order::where('id',$order->id)->first();

        $order_number=$order->order_number;
        $products = OrderProduct::where('order_id',$order->id)->get();

        $data['data'] =[
            'order_data'=> new OrderResource([ 'order' => $order, 'order_number' => $order_number, 'address' => $address,  ]),
            'order_products' => OrderStoreResource::collection($products),
        ] ;
        $data['message'] = trans('api.pending_order');
        $data['status'] = 'ok';
        return response()->json($data, 200);
    }


    public function delete(OrderDeleteRequest $request)
    {
        $order = Order::find($request->order_id)->first();
      //  $order = Order::where('order_number', $orderNumber)->first();
        if (is_null($order)) {
            $data['data'] = [];
            $data['message'] = trans('api.data_not_found');
            $data['status'] = 'fails';
            return response()->json($data, 405);
        }
      //  $order->bag->delete();
        $order->delete();

        $data['data'] = '';
        $data['message'] = trans('api.deleted_successfully');
        $data['status'] = 'ok';
        return response()->json($data, 200);
    }

    public function orderDetails(OrderDeleteRequest $request)
    {
        $order = Order::find($request->order_id)->first();
        if (is_null($order)) {
            $data['data'] = [];
            $data['message'] = trans('api.data_not_found');
            $data['status'] = 'fails';
            return response()->json($data, 405);
        }
        $data['data'] = new OrderDetailsResource($order);
        $data['message'] = trans('api.deleted_successfully');
        $data['status'] = 'ok';
        return response()->json($data, 200);
    }


    public function evaluate(OrderEvaluateRequest $request)
    {
       $client_id= auth()->user()->id;
        $order = Order::where(['id'=>$request->order_id,'client_id'=>auth()->user()->id])->first();
        if (is_null($order)) {
            $data['data'] = [];
            $data['message'] = trans('api.data_not_found');
            $data['status'] = 'fails';
            return response()->json($data, 405);
        }
        $order->update(['order_rate' => $request->order_rate, 'order_review' => $request->order_review,]);
       $order_products=$order->OrderProducts ;
        foreach ($order_products as $key) {
            $product_rate = ProductRate:: firstOrCreate([
                'product_id' => $key->product_id,
                'provider_id' => $key->provider_id,
                'client_id' => $client_id,
                'rate' => $request->order_rate,
            ]);
           $product_rates=ProductRate::where('product_id', $key->product_id)->get();
            foreach ($product_rates as $product_rate) {
                $product_avg_rate = (int) $product_rate->avg('rate');
            }
            Product::where('id',$product_rate->product_id)->update(['rate_avg' =>$product_avg_rate]);
        }
        $data['data'] = '';
        $data['message'] = trans('api.added_successfully');
        $data['status'] = 'ok';
        return response()->json($data, 200);
    }




}
