<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PARENT_API;
use App\Http\Requests\api\ProductDeatilsRequest;
use App\Http\Resources\ProductDetailsResource;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends PARENT_API
{
    public function show(ProductDeatilsRequest $request)
    {
        $product = Product::where('id', $request->product_id)->first();

        $product->views_num += 1;
        $product->update();

        $this->data['data'] = new ProductDetailsResource($product);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }
}
