<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\FilterRequest;
use App\Http\Controllers\PARENT_API;
use App\Http\Resources\api\CompanyResource;
use App\Http\Resources\api\ModelResource;
use App\Http\Resources\api\PriceResource;
use App\Http\Resources\api\ProductFilterResource;
use App\Http\Resources\api\ProductionYearResource;
use App\Http\Resources\api\SerialResource;
use App\Http\Resources\api\UserResource;
use App\Product;
use Illuminate\Http\Request;

class FilterController extends PARENT_API
{

    public function company(Request $request)
    {
        $company = Product::all();
        $this->data['data'] = new CompanyResource($company);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }

    public function model(Request $request)
    {
        $model = Product::all();
        $this->data['data'] = new ModelResource($model);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }

    public function serial(Request $request)
    {
        $serial = Product::all();
        $this->data['data'] = new SerialResource($serial);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }

    public function production_year(Request $request)
    {
        $serial = Product::all();
        $this->data['data'] = new ProductionYearResource($serial);
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }

    public function price (Request $request)
    {
        $max_price = Product::max('price');
        $min_price = Product::min('price');
        $this->data['data'] =['max_price'=>$max_price,'min_price'=>$min_price];
        $this->data['message'] = '';
        $this->data['status'] = 'ok';
        return response()->json($this->data, 200);
    }


    public function Filter(FilterRequest $request )
    {

        $max_price = Product::max('price');
        $min_price = Product::min('price');

        $products=Product::
        when($request->company_name ,function($q) use ($request){
            $q->where('company_name','like','%'.$request['company_name'].'%');
        })->when($request->model ,function($q) use ($request){
            $q->where('model', $request['model']);
        })->when($request->production_year ,function($q) use ($request){
            $q->where('production_year', $request['production_year']);
        })->when($request->status ,function($q) use ($request){
            $q->where('status', $request['status']);
        })->when($request->serial ,function($q) use ($request){
            $q->where('serial', $request['serial']);
        })->when($request->price ,function($q) use ($request,$max_price,$min_price){
            $q-> whereBetween('price', [$min_price, $max_price]);
        })->get();


        $data['data'] = ['products' => ProductFilterResource::collection($products)];
        $data['message'] = 'ok';
        $data['status'] = 'ok';

        return response()->json($data, 200);
    }
}
