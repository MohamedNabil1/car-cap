<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    public static function upload_single($file, $path)
    {
        $name = (time() * rand(1, 99)) . '.' . $file->getClientOriginalExtension();
        Image::make($file)->save('public/' . $path . $name);
        return $name;
    }

    public static function delete_image($image_name, $path)
    {
        if ($image_name != 'default.png') {
            Storage::disk('public')->delete($path . '/' . $image_name);
        }
    }
}
