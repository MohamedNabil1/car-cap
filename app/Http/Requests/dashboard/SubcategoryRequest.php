<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class SubcategoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }
            case 'POST': {

                    return [
                        'name_ar' => 'required',
                        'name_en' => 'required',
                        'description_ar' => 'sometimes',
                        'description_en' => 'sometimes',
                        'category_id' => 'required',
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'name_ar' => 'required',
                        'name_en' => 'required',
                        'description_ar' => 'sometimes',
                        'description_en' => 'sometimes',
                        'category_id' => 'required',
                    ];
                }
            default:
                break;
        }
    }
}
