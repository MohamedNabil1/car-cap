<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }

            case 'POST': {

                    return [
                        'user_id' => 'sometimes',
                        'name_ar' => 'required',
                        'name_en' => 'required',
                        'company_name' => 'required',
                        'description_ar' => 'required',
                        'description_en' => 'required',
                        'status' => 'required|in:new,used',
                        'model' => 'required',
                        'stock_count' => 'required',
                        'production_year' => 'required',
                        'price' => 'required|numeric',
                        'city_id' => 'required',
                        'images' => 'required',
                        'images.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
                    ];
                }
            case 'PUT':
            case 'PATCH': {
                    return [
                        'name_ar' => 'required',
                        'name_en' => 'required',
                        'company_name' => 'required',
                        'description_ar' => 'required',
                        'description_en' => 'required',
                        'status' => 'required|in:new,used',
                        'model' => 'required',
                        'production_year' => 'required',
                        'price' => 'required|numeric',
                        'city_id' => 'required',
                    ];
                }
            default:
                break;
        }
    }
}
