<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class AddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch ($this->method()) {
            case 'GET':
            case 'DELETE': {
                    return [];
                }

            case 'POST': {

                    return [
                        'address' => 'required',
                        'type_of_address' => 'required',
                        'depart_number' => 'required',
                        'mobile' => 'required',
                        'city_id' => 'required|exists:cities,id',
                    ];
                }
            case 'PUT':

            case 'PATCH': {
                    return [
                        'address' => 'required',
                        'type_of_address' => 'required',
                        'depart_number' => 'required',
                        'mobile' => 'required',
                        'city_id' => 'required|exists:cities,id',
                    ];
                }

            default:
                break;
        }
    }
}
