<?php

namespace App\Http\Requests\dashboard;

use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        switch ($this->method()) {

            case 'POST': {
                    return [

                        'full_name' => 'required',
                        'email' => 'required|unique:users',
                        'image' => 'sometimes|image',
                        'password' => 'required|min:6|confirmed',
                        'mobile' => 'required',
                    ];
                }

            case 'PUT': {
                    $user = $this->admin->id;

                    return [

                        'full_name' => 'sometimes',
                        'email' => ['sometimes', 'string', 'email', 'max:255', 'unique:users,email,' . $user],
                        'image' => 'sometimes|image',
                        'password' => 'required|min:3|confirmed',
                        'city_id' => 'sometimes',
                        'mobil' => 'sometimes',

                    ];
                }
            default:
                break;
        }
    }
    public function attributes()
    {
        return [
            'type' => 'نوع المستخدم',
            'full_name' => 'الاسم بالكامل',
            'image' => 'الصوره الشخصيه',
            'address' => ' العنوان',
            'type_of_address' =>  'نوع العنوان',
            'depart_number' => ' رقم الشقه',
        ];
    }
}
