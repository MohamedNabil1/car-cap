<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        switch ($this->method()) {

            case 'POST': {
                    return [
//                        'first_name' => 'required',
//                        'last_name' => 'required',
                        'full_name' => 'required',
                        'email' => 'required|unique:users',
                        'image' => 'sometimes|image',
                        'password' => 'required|min:8|confirmed',
                        'mobile' => 'required',
                        'address' => 'required',
                        'type_of_address' => 'required',
                        'area' => 'required',
                        'depart_number' => 'required',
                    ];
                }

            case 'PUT': {
                    $user = $this->user->id;

                    return [
//                        'first_name' => 'required',
//                        'last_name' => 'required',
                        'full_name' => 'sometimes',
                        'email' => ['sometimes', 'string', 'email', 'max:255', 'unique:users,email,' . $user],
                       // 'type' => 'required',
                        'image' => 'sometimes|image',
                        'password' => 'required|min:8|confirmed',
                        'city_id' => 'sometimes',
                        'mobil' => 'sometimes',
                        'address' => 'sometimes',
                        'type_of_address' => 'sometimes',
                        'depart_number' => 'sometimes',
                    ];
                }
            default:
                break;
        }
    }
    public function attributes()
    {
        return [
            'type' => 'نوع المستخدم',
            'full_name' => 'الاسم بالكامل',
            'image' => 'الصوره الشخصيه',
            'address' => ' العنوان',
            'type_of_address' =>  'نوع العنوان',
            'depart_number' => ' رقم الشقه',
        ];
    }
}
