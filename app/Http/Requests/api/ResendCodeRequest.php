<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;

class ResendCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mobile' => 'required||regex:/(9665)[0-9]{8}/|exists:user,mobile',
            'type' => 'required|in:active,reset',
        ];
    }
}
