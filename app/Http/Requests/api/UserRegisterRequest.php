<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\REQUEST_API_PARENT;

class UserRegisterRequest extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            // 'first_name' => 'required',
            // 'last_name' => 'required',
            'full_name' => 'required',
            'email' => 'required|unique:users',
            // 'type' => 'required', //client by Default
            'city_id' => 'required|numeric|exists:cities,id',
            'image' => 'sometimes|image',
            'password' => 'required|min:8|confirmed',
            'fcm_token'              => 'required|string',

             'mobile' => 'required|unique:users,mobile',
            // 'area' => 'required',
        ];
    }
}
