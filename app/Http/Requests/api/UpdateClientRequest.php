<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\REQUEST_API_PARENT;
use Illuminate\Http\Request;


class UpdateClientRequest extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        return [
            'full_name' => 'string|max:255',
            'mobile' => 'nullable|string|min:9|max:255',
            'email' => 'email|max:255|unique:users,email,' . auth()->user()->id,
            'city_id' => 'numeric|exists:cities,id',
//            'image' => 'sometimes|image',
            'image' => 'nullable|image|mimes:jpg,png,jpeg',


        ];
    }
}
