<?php

namespace App\Http\Requests\api;

use Illuminate\Foundation\Http\FormRequest;

use App\Http\Requests\REQUEST_API_PARENT;

class FilterRequest extends REQUEST_API_PARENT
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           // 'key' => 'required|in:company_name,model,production_year,status,serial,price',
           // 'key' => 'required',
            //'value' => 'required',
           // 'value' => 'required|in:company_name,model,production_year,status,serial',

            'company_name'=>'nullable',
            'model'=>'nullable',
            'production_year'=>'nullable',
            'status'=>'nullable',
            'serial'=>'nullable',
            'price'=>'nullable',




        ];
    }
}
