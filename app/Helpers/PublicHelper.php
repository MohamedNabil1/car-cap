<?php

use App\Setting;

function SETTING_VALUE($key = false)
{
    return Setting::where('key', $key)->first()->value;
}
