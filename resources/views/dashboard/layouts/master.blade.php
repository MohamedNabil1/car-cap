<!DOCTYPE html>
<html>

<head>
    @include('dashboard.layouts.includes.head')
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <header class="main-header">
            @include('dashboard.layouts.includes.header')
        </header>

        <aside class="main-sidebar">
            @include('dashboard.layouts.includes.aside')
        </aside>

        <div class="content-wrapper">

            <section class="content-header">
                @yield('header')
            </section>


            <section class="content">
                @yield('content')
            </section>

        </div>

        <footer class="main-footer">
            @include('dashboard.layouts.includes.footer')
        </footer>

    </div>

    @include('dashboard.layouts.includes.scripts')

</body>

</html>