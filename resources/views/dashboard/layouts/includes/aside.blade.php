<section class="sidebar">
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{ auth()->user()->ImagePath }}  " class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p> {{ auth()->user()->full_name }} </p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">{{ trans('site.main_menu') }}</li>
        <li>
            <a href=" {{route('dashboard.index')}} ">
                <i class="fa fa-dashboard"></i> <span> @lang('site.dashboard')</span>
                </span>
            </a>
        </li>


        <!-- My Profile -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>الملف الشخصي</span>
                    <span class="pull-right-container">
                             <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        @if (auth()->user()->hasRole('admin'))
                            <a href="{{ route('admins.show',auth()->user()->id) }}" class="fa fa-circle-o "> Profile</a>
                        @else
                            <a href="{{ route('providers.show',auth()->user()->id) }}" class="fa fa-circle-o "> Profile</a>
                        @endif
                    </li>
                </ul>
            </li>
    <!-- End My Profile  -->


        <!-- All Providers -->
        @if (auth()->user()->hasRole('admin'))
            @if(auth()->user()->hasPermission('read_users'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('site.providers')</span>
                        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('providers.index')}} "><i
                                        class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                        @if(auth()->user()->hasPermission('create_users'))
                            <li><a href="{{route('providers.create')}} "><i
                                            class="fa fa-circle-o"></i> @lang('site.create')
                                </a></li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
    <!-- End All Providers  -->

        <!-- All Users  -->
        @if (auth()->user()->hasRole('admin'))

            @if(auth()->user()->hasPermission('read_users'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('site.users')</span>
                        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('users.index')}} "><i
                                        class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                        @if(auth()->user()->hasPermission('create_users'))
                            <li><a href="{{route('users.create')}} "><i class="fa fa-circle-o"></i> @lang('site.create')
                                </a></li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
    <!-- End All users  -->

        <!-- All City -->
        @if (auth()->user()->hasRole('admin'))

            @if(auth()->user()->hasPermission('read_cities'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('site.cities')</span>
                        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('city.index')}}"><i
                                        class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                        @if(auth()->user()->hasPermission('create_cities'))
                            <li><a href="{{route('city.create')}} "><i class="fa fa-circle-o"></i> @lang('site.create')
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
    <!--End All City  -->

        <!-- All Category -->
        @if (auth()->user()->hasRole('admin'))
            @if(auth()->user()->hasPermission('read_categories'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('site.categories')</span>
                        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('categories.index')}}"><i
                                        class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                        @if(auth()->user()->hasPermission('create_categories'))
                            <li><a href="{{route('categories.create')}} "><i
                                            class="fa fa-circle-o"></i> @lang('site.create') </a></li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
    <!--End All Category  -->


        <!-- All products  -->
        @if (auth()->user()->hasRole('admin'))
            @if(auth()->user()->hasPermission('read_products'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('site.products')</span>
                        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('products.index')}}"><i
                                        class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                        @if(auth()->user()->hasPermission('create_products'))
                            <li><a href="{{route('products.create')}} "><i
                                            class="fa fa-circle-o"></i> @lang('site.create')
                                </a></li>
                        @endif
                    </ul>
                </li>
            @endif
        @endif
    <!-- End All products -->


        <!-- All orders -->
        @if (auth()->user()->hasRole('admin'))
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>{{trans('site.all-orders')}}</span>
                    <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{route('orders.index')}}"><i
                                    class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                </ul>
            </li>
        @endif
    <!--End All orders -->

        <!-- All Advertises -->

        @if (auth()->user()->hasRole('admin'))
            <li class="treeview">
                <a href="{{ route('advertises.index') }}">
                    <i class="fa fa-dashboard"></i> <span>{{trans('site.advertises')}}</span>
                    <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="{{route('advertises.index')}}"><i
                                    class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                    <li class="active"><a href="{{route('advertises.create')}}"><i
                                    class="fa fa-circle-o"></i> @lang('site.add')</a></li>
                </ul>
            </li>
        @endif
    <!--End All Advertises -->

        <!-- All Settings Admin -->
        @if (auth()->user()->hasRole('admin'))

            @if(auth()->user()->hasPermission('update_settings'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('site.settings')</span>
                        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('settings.index')}}"><i
                                        class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                    </ul>
                </li>
            @endif
        @endif
    <!--End All Settings Admin -->

        <!-- All Settings Providers -->
        {{--<li class="treeview">--}}
        {{--<a href="#">--}}
        {{--<i class="fa fa-dashboard"></i> <span>@lang('site.app_links')</span>--}}
        {{--<span class="pull-right-container">--}}
        {{--<i class="fa fa-angle-left pull-right"></i>--}}
        {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu">--}}
        {{--<li class="active"><a href="{{route('privacy')}}"><i class="fa fa-circle-o"></i> @lang('site.privacy_policy')</a></li>--}}
        {{--<li><a href="{{route('terms')}}"><i class="fa fa-circle-o"></i> @lang('site.terms')</a></li>--}}
        {{--<li><a href="{{route('about')}} "><i class="fa fa-circle-o"></i> @lang('site.about_app') </a></li>--}}

        {{--</ul>--}}
        {{--</li>--}}
    <!--End All Settings Providers -->

        <!-- All  Admins -->
        @if (auth()->user()->hasRole('admin'))
            @if(auth()->user()->hasPermission('read_users'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>@lang('site.admins')</span>
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                       </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('admins.index')}} "><i
                                        class="fa fa-circle-o"></i> @lang('site.preview_all')</a></li>
                        @if(auth()->user()->hasPermission('create_users'))
                            <li><a href="{{route('admins.create')}} "><i
                                            class="fa fa-circle-o"></i> @lang('site.create')
                                </a></li>
                        @endif
                    </ul>
                </li>
        @endif
    @endif
    <!--End All  Admins -->

    </ul>
</section>