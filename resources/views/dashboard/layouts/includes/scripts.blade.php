<script src="{{ asset('public/dashboard_files/bower_components/jquery/dist/jquery.min.js') }}  "></script>
<!-- jQuery UI 1.11.4 -->
<script src=" {{ asset('public/dashboard_files/bower_components/jquery-ui/jquery-ui.min.js') }}  "></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src=" {{ asset('public/dashboard_files/bower_components/bootstrap/dist/js/bootstrap.min.js') }}  "></script>
<!-- Morris.js charts -->
<script src=" {{ asset('public/dashboard_files/bower_components/raphael/raphael.min.js') }}  "></script>
<script src=" {{ asset('public/dashboard_files/bower_components/morris.js/morris.min.js') }}  "></script>
<!-- Sparkline -->
<script src=" {{ asset('public/dashboard_files/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}  "></script>
<!-- jvectormap -->
<script src=" {{ asset('public/dashboard_files/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}  "></script>
<script src=" {{ asset('public/dashboard_files/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}  "></script>
<!-- jQuery Knob Chart -->
<script src=" {{ asset('public/dashboard_files/bower_components/jquery-knob/dist/jquery.knob.min.js') }}  "></script>
<!-- daterangepicker -->
<!-- datepicker -->
<script src=" {{ asset('public/dashboard_files/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}  "></script>
<!-- Bootstrap WYSIHTML5 -->
<script src=" {{ asset('public/dashboard_files/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}  "></script>
<!-- Slimscroll -->
<script src=" {{ asset('public/dashboard_files/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}  "></script>
<!-- FastClick -->
<script src=" {{ asset('public/dashboard_files/bower_components/fastclick/lib/fastclick.js') }}  "></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/dashboard_files/js/adminlte.min.js') }}  "></script>
<script src="{{ asset('public/dashboard_files/js/noty.min.js') }}  "></script>


<script>
  //delete
  $('.delete').click(function(e) {

    var that = $(this)

    e.preventDefault();

    var n = new Noty({
      text: "@lang('site.confirm_delete')",
      type: "warning",
      killer: true,
      buttons: [
        Noty.button("@lang('site.yes')", 'btn btn-success mr-2', function() {
          that.closest('form').submit();
        }),

        Noty.button("@lang('site.no')", 'btn btn-primary mr-2', function() {
          n.close();
        })
      ]
    });

    n.show();

  }); //end of delete


  // image preview
  $(".image").change(function() {

    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('.image-preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(this.files[0]);
    }

  });
</script>