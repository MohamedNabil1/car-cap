<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Car-Cap| Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('public/dashboard_files/bower_components/bootstrap/dist/css/bootstrap.min.css') }}  ">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('public/dashboard_files/bower_components/Ionicons/css/ionicons.min.css') }}  ">
<!-- skins -->
<link rel="stylesheet" href="{{ asset('public/dashboard_files/css/skins/_all-skins.min.css') }}  ">
<!-- Morris chart -->
<link rel="stylesheet" href="{{ asset('public/dashboard_files/bower_components/morris.js/morris.css') }}  ">
<link rel="stylesheet" href="{{ asset('public/dashboard_files/css/noty.css') }}  ">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<script src="https://kit.fontawesome.com/e0a17cc3c7.js" crossorigin="anonymous"></script>


@if(app()->getlocale()== 'ar' )

<link rel="stylesheet" href="{{ asset('public/dashboard_files/css/AdminLTE-rtl.min.css') }}">
<link href="https://fonts.googleapis.com/css?family=Cairo:400,700" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('public/dashboard_files/css/bootstrap-rtl.min.css') }}">
<link rel="stylesheet" href="{{ asset('public/dashboard_files/css/rtl.css') }}">

@else
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link rel="stylesheet" href="{{ asset('public/dashboard_files/css/AdminLTE.min.css') }}">

@endif