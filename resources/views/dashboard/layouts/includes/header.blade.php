 <!-- Logo -->
 <a href="index2.html" class="logo">
   <!-- mini logo for sidebar mini 50x50 pixels -->
   <span class="logo-mini"><b>C</b>ar</span>
   <!-- logo for regular state and mobile devices -->
   <span class="logo-lg"><b>Car</b>Cap</span>
 </a>
 <!-- Header Navbar: style can be found in header.less -->
 <nav class="navbar navbar-static-top">
   <!-- Sidebar toggle button-->
   <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
     <span class="sr-only">Toggle navigation</span>
   </a>

   <div class="navbar-custom-menu">
     <ul class="nav navbar-nav">
       <!-- Messages: style can be found in dropdown.less-->
       <li class="dropdown messages-menu">
         {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
           {{--<i class="fa fa-envelope-o"></i>--}}
           {{--<span class="label label-success">4</span>--}}
         {{--</a>--}}
         <ul class="dropdown-menu">
           {{--<li class="header">You have 4 messages</li>--}}
           {{--<li>--}}
             {{--<!-- inner menu: contains the actual data -->--}}
             {{--<ul class="menu">--}}
               {{--<li>--}}
                 {{--<!-- start message -->--}}
                 {{--<a href="#">--}}
                   {{--<div class="pull-left">--}}
                     {{--<img src=" {{ asset('public/dashboard_files/img/user2-160x160.jpg') }} " class="img-circle" alt="User Image">--}}
                   {{--</div>--}}
                   {{--<h4>--}}
                     {{--Support Team--}}
                     {{--<small><i class="fa fa-clock-o"></i> 5 mins</small>--}}
                   {{--</h4>--}}
                   {{--<p>Why not buy a new awesome theme?</p>--}}
                 {{--</a>--}}
               {{--</li>--}}
               {{--<!-- end message -->--}}
               {{--<li>--}}
                 {{--<a href="#">--}}
                   {{--<div class="pull-left">--}}
                     {{--<img src=" {{ asset('public/dashboard_files/img/user3-128x128.jpg') }} " class="img-circle" alt="User Image">--}}
                   {{--</div>--}}
                   {{--<h4>--}}
                     {{--Sales Department--}}
                     {{--<small><i class="fa fa-clock-o"></i> Yesterday</small>--}}
                   {{--</h4>--}}
                   {{--<p>Why not buy a new awesome theme?</p>--}}
                 {{--</a>--}}
               {{--</li>--}}
               {{--<li>--}}
                 {{--<a href="#">--}}
                   {{--<div class="pull-left">--}}
                     {{--<img src=" {{ asset('public/dashboard_files/img/user4-128x128.jpg') }} " class="img-circle" alt="User Image">--}}
                   {{--</div>--}}
                   {{--<h4>--}}
                     {{--Reviewers--}}
                     {{--<small><i class="fa fa-clock-o"></i> 2 days</small>--}}
                   {{--</h4>--}}
                   {{--<p>Why not buy a new awesome theme?</p>--}}
                 {{--</a>--}}
               {{--</li>--}}
             {{--</ul>--}}
           {{--</li>--}}
           {{--<li class="footer"><a href="#">See All Messages</a></li>--}}
         </ul>
       </li>
       <!-- Notifications: style can be found in dropdown.less -->
       <li class="dropdown notifications-menu">
         {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
           {{--<i class="fa fa-bell-o"></i>--}}
           {{--<span class="label label-warning">{{ auth()->user()->notifications->count() }}</span>--}}
         {{--</a>--}}
         {{--<ul class="dropdown-menu">--}}
           {{--@foreach(App\Notification::where('notified_user_id' , auth()->user()->id)->get() as $notification)--}}
           {{--<li class="header">{{ $notification->title ?? trans('site.no_data_found') }}</li>--}}
           {{--@endforeach--}}
           {{--<li class="footer"><a href="#">View all</a></li>--}}
         {{--</ul>--}}
       </li>
       <!-- Tasks: style can be found in dropdown.less -->
       <li class="dropdown tasks-menu">
         {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
           {{--<i class="fa fa-flag-o"></i>--}}
           {{--<span class="label label-danger">9</span>--}}
         {{--</a>--}}
         {{--<ul class="dropdown-menu">--}}
         {{--</ul>--}}
       </li>
       <!-- User Account: style can be found in dropdown.less -->
       <li class="dropdown user user-menu">
         <a href="#" class="dropdown-toggle" data-toggle="dropdown">
           <img src=" {{ auth()->user()->ImagePath }}  " class="user-image" alt="User Image">
           <span class="hidden-xs">{{ auth()->user()->full_name }}</span>
         </a>
         <ul class="dropdown-menu">
           <!-- User image -->
           <li class="user-header">
             <img src=" {{ auth()->user()->ImagePath }} " class="img-circle" alt="User Image">

             <p>
               {{auth()->user()->full_name}} - {{auth()->user()->type}}

             </p>
           </li>
           <!-- Menu Body -->
           <li class="user-body">
             <div class="row">
               <div class="col-xs-4 text-center">
                 {{--<a href="{{ route('pending.products') }}">{{ trans('site.pending_products') }}</a>--}}
               </div>
               {{--<div class="col-xs-4 text-center">--}}
                 {{--<a href="#">Sales</a>--}}
               {{--</div>--}}
               {{--<div class="col-xs-4 text-center">--}}
                 {{--<a href="#">Friends</a>--}}
               {{--</div>--}}
             </div>
             <!-- /.row -->
           </li>
           <!-- Menu Footer-->
           <li class="user-footer">
             <div class="pull-left">
               @if (auth()->user()->hasRole('admin'))
               <a href="{{ route('admins.show',auth()->user()->id) }}" class="btn btn-default btn-flat">Profile</a>
                 @else
                 <a href="{{ route('providers.show',auth()->user()->id) }}" class="btn btn-default btn-flat">Profile</a>
               @endif
             </div>
             <div class="pull-right">
               <form action="{{ route('logout') }}" method="POST">
                 @csrf
                 <button type="submit">@lang('site.logout')</button>
               </form>
             </div>
           </li>
         </ul>
       </li>
       <!-- Control Sidebar Toggle Button -->
       <li>
         <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
       </li>
     </ul>
   </div>
 </nav>