@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.dashboard')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="#"><a href="{{ route('admins.index') }}">@lang('site.admins')</a></li>
</ol>
@endsection

@include('dashboard.layouts.includes.errors')

<div class="row">
    <div class="col-md-6">
        <form action=" {{ route('admins.update',$admin) }} " method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="box-body">



                <div class="form-group">

                    <input type="text" class="form-control" value="{{$admin->full_name}}" name="full_name" placeholder="@lang('site.full_name')">
                </div>

                <div class="form-group">

                    <input type="email" class="form-control" value="{{$admin->email}}" name="email" placeholder="@lang('site.email')">
                </div>


                <div class="form-group">
                    <select name="city_id" id="" class="form-control">
                        <option value="" disabled selected> {{trans('site.cities')}} </option>
                        @foreach($cities as $city)
                            <option value="{{$city->id}}">{{$city->name_ar}}-{{$city->name_en}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>@lang('site.image')</label>
                    <input type="file" class="form-control image " name="image">
                </div>

                <div class="form-group">
                    <img src=" {{ asset('public/uploads/default.png') }} " width=" 100px " class="thumbnail image-preview">
                </div>

                <div class="form-group">

                    <input type="password" class="form-control" name="password" placeholder="@lang('site.password')">
                </div>

                <div class="form-group">

                    <input type="password" class="form-control" name="password_confirmation" placeholder="@lang('site.password_confirmation')">

                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-edit"></i> @lang('site.edit')</button>
                </div>


            </div>
        </form>
    </div>




</div>


@endsection