@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.dashboard')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

        <li class="#"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
        <li class="active"><a href="{{ route('admins.index') }}">@lang('site.admins')</a></li>
        <li class="active"><a href="{{ route('admins.create') }}">@lang('site.create_admins')</a></li>
    </ol>
@endsection

@include('dashboard.layouts.includes.errors')

<form action=" {{ route('admins.store') }} " method="POST" enctype="multipart/form-data">
    @csrf

    <div class="box-body">
        <div>
            <label for=""> @lang('site.main_info') </label>

            <div class="form-group">

                <input type="text" class="form-control" name="full_name" placeholder="@lang('site.full_name')">
            </div>

            <div class="form-group">

                <input type="email" class="form-control" name="email" placeholder="@lang('site.email')">
            </div>

            <div class="form-group">

                <input type="text" class="form-control" name="mobile" placeholder="@lang('site.mobile')">
            </div>

            <div class="form-group">
                <select name="city_id" id="" class="form-control">
                    <option value="" disabled selected> {{trans('site.cities')}} </option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name_ar}}-{{$city->name_en}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="@lang('site.password')">
            </div>

            <div class="form-group">

                <input type="password" class="form-control" name="password_confirmation"
                       placeholder="@lang('site.password_confirmation')">

            </div>

            <div class="form-group">
                <label>@lang('site.image')</label>
                <input type="file" class="form-control image " name="image">
            </div>

            <div class="form-group">
                <img src=" {{ asset('public/uploads/default.png') }} " width=" 100px " class="thumbnail image-preview">
            </div>

            <div class="form-group">
                <label for="">@lang('site.permissions')</label>
                @php
                    $models = ['users','cities','categories','products',];
                    $maps =['create','read','update','delete'];
                @endphp
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        @foreach($models as $index=>$model)
                            <li class="{{ $index == 0 ? 'active' : '' }}"><a href="#{{$model}}"
                                                                             data-toggle="tab"> @lang('site.'.$model)</a>
                            </li>
                        @endforeach
                    </ul>
                    <div class="tab-content">
                        @foreach($models as $index=>$model)
                            <div class="tab-pane {{ $index == 0 ? 'active' : '' }}" id="{{$model}}">
                                @foreach($maps as $map)
                                    <label for=""> <input type="checkbox" name="permissions[]"
                                                          value="{{$map}}_{{$model}}"> @lang('site.'.$map) </label><br>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="box-footer">

        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> {{trans('site.add')}}</button>

    </div>


</form>

@endsection