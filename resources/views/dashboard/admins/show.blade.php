@extends('dashboard.layouts.master')
@section('content')
@section('header')
    <h1>
        @lang('site.dashboard')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">
    </ol>
@endsection

<!-- Content Wrapper. Contains page content -->

<section class="content-header">
    @include('dashboard.layouts.includes.errors')
    <h1>
        {{$user->full_name}}
    </h1>
    <ol class="breadcrumb">
        <li class="#"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i>@lang('site.dashboard')
            </a></li>
        <li class="active"><a href="{{ route('admins.index') }}">@lang('site.admins')</a></li>
    </ol>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-3">
            <!------------------------------------------------------- start about ---------------------------------------->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{$user->ImagePath}}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center"> {{$user->full_name}} </h3>

                    <p class="text-muted text-center">{{$user->type}} </p>

                    <ul class="list-group list-group-unbordered">

                        <li class="list-group-item">
                            <b>@lang('site.user_type')</b> <a class="pull-right">{{ $user->type }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--------------------------------------------- end About -------------------------------------->

        <!------------------------------------ Tabs -------------------------------->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">@lang('site.main_info')</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="box-body">
                            @if($user->count() > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> @lang('site.full_name') </th>
                                        <th> @lang('site.email') </th>
                                        <th> @lang('site.mobile') </th>
                                        <th> @lang('site.type') </th>
                                        <th> @lang('site.city') </th>
                                        <th> @lang('site.image') </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td> {{ $user->full_name }} </td>
                                        <td> {{ $user->email }} </td>
                                        <td> {{ $user->mobile }} </td>
                                        <td> {{ $user->type }} </td>
                                        @if($user->city_id != null)
                                            <td> {{ $user->city->name_ar }}-{{ $user->city->name_en }} </td>
                                        @else
                                            <td> Admin City</td>
                                        @endif
                                        <td><img src="{{ $user->ImagePath }}" style="width: 80px ; height:80px"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
<!-- /.content -->

@endsection