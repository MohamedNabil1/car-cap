@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.dashboard')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('categories.index') }}">@lang('site.categories')</a></li>
    <li class="active"><a href="{{ route('categories.edit',$category) }}">@lang('site.edit')</a></li>
</ol>
@endsection



@include('dashboard.layouts.includes.errors')
<form action="{{ route('categories.update',$category) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="box-body">
        <div class="form-group">
            <input type="text" class="form-control" value="{{$category->name_ar}}" name="name_ar" placeholder="@lang('site.name_ar') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$category->name_en}}" name="name_en" placeholder="@lang('site.name_en') ">
        </div>
        <div class="form-group">
            <label>@lang('site.image')</label>
            <input type="file" class="form-control image" name="image">
        </div>
    </div>

    <div class="box-footer">
        <button type="submit">{{trans('site.edit')}}</button>
    </div>
</form>





@endsection