@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.categories')
    <small>@lang('site.preview_all') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('categories.index') }}">@lang('site.categories')</a></li>
</ol>
@endsection
<div class="box-header with-border">
    <h3 class="box-title mb-3"> @lang('site.categories') </h3> <small>{{$categories->count() != null ? $categories->count() : 0 }}</small>
    <form action="{{ route('categories.index') }}" method="get">
        <div class="row">
            <div class="col-md-4">
                <input type="text" class="form-control" name="search" placeholder="search" value="{{request()->search}}">
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> Search</button>
                <a href="{{ route('categories.create') }}" class="btn btn-info"> {{trans('site.add')}} </a>
            </div>
        </div>
    </form>
</div>

@include('dashboard.layouts.includes.errors')

<div class="box-body">
    @if($categories->count() > 0)

    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th> # </th>
                <th> @lang('site.name_ar') </th>
                <th> @lang('site.name_en') </th>
                <th> @lang('site.number_inner_prducts') </th>
                <th> @lang('site.image') </th>
                @if(auth()->user()->hasRole('super_admin'))
                <th> @lang('site.action') </th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $index=>$category )
            <tr>
                <td> {{ $index+1 }} </td>
                <td> {{ $category->name_ar }} </td>
                <td> {{ $category->name_en }} </td>
                <td> {{ count($category->products) }} </td>
                <td><img src="{{ $category->ImagePath }}" style="width: 80px ; height:80px"></td>
                @if(auth()->user()->hasRole('super_admin'))
                <td>
                    <a href=" {{route('categories.edit',$category->id)}} " class="btn btn-info btn-sm"> <i class="fas fa-edit"></i> @lang('site.edit') </a>
                    {{--<a href=" {{route('categories.show',$category->id)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> @lang('site.see_more') </a>--}}
                    <form action=" {{ route('categories.destroy',$category->id) }} " method="POST" style="display:inline-block">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                    </form>
                </td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
    @else

    <h2> @lang('site.no_data_found') </h2>

    @endif

</div>





@endsection