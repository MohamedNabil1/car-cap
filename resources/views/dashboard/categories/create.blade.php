@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.categories')
    <small>@lang('site.create') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li><a href="{{ route('categories.index') }}">@lang('site.categories')</a></li>
    <li class="active"><a href="{{ route('categories.index') }}">@lang('site.create')</a></li>
</ol>
@endsection
@include('dashboard.layouts.includes.errors')
<div class="row">
    <div class="col-md-6">
        <form action="{{ route('categories.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <input type="text" class="form-control" name="name_ar" placeholder="@lang('site.name_ar') ">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="name_en" placeholder="@lang('site.name_en') ">
                </div>
                <div class="form-group">
                    <label>@lang('site.image')</label>
                    <input type="file" class="form-control image " name="image">
                </div>

                <div class="form-group">
                    <img src=" {{ asset('public/uploads/default.png') }} " width=" 100px " class="thumbnail image-preview">
                </div>
            </div>

            <div class="box-footer">
                <input type="submit" name="add" value="{{trans('site.add')}}">
                <input type="submit" name="addNew" value="{{trans('site.addNew')}}">
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <label for="table"> {{@trans('site.last_added')}} </label>
        <div class="box-body">
            @if($categories->count() > 0)

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> @lang('site.name_ar') </th>
                        <th> @lang('site.name_en') </th>
                        <th> @lang('site.number_inner_prducts') </th>
                        <th> @lang('site.image') </th>
                        <th> @lang('site.action') </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($categories as $index=>$category )
                    <tr>
                        <td> {{ $index+1 }} </td>
                        <td> {{ $category->name_ar }} </td>
                        <td> {{ $category->name_en }} </td>
                        <td> {{ count($category->products) }} </td>
                        <td><img src="{{ $category->ImagePath }}" style="width: 80px ; height:80px"></td>
                        <td>
                            <a href=" {{route('categories.edit',$category->id)}} " class="btn btn-info btn-sm"> <i class="fas fa-edit"></i> @lang('site.edit') </a>
                            <a href=" {{route('categories.show',$category->id)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> @lang('site.see_more') </a>
                            <form action=" {{ route('categories.destroy',$category->id) }} " method="POST" style="display:inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else

            <h2> @lang('site.no_data_found') </h2>

            @endif

        </div>
    </div>
</div>







@endsection