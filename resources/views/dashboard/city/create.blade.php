@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.dashboard')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('city.index') }}">@lang('site.cities')</a></li>
    <li class="active"><a href="{{ route('city.create') }}">@lang('site.create_city')</a></li>
</ol>
@endsection

@include('dashboard.layouts.includes.errors')

<div class="row">
    <div class="col-md-6">
        <form action=" {{ route('city.store') }} " method="POST">
            @csrf

            <div class="form-group">
                <input type="text" class="form-control" name="name_ar" placeholder="@lang('site.name_ar')">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" name="name_en" placeholder="@lang('site.name_en')">
            </div>

            <div class="box-footer">
                <input type="submit" class="btn btn-primary" name="add" value="{{trans('site.add')}}">
                <input type="submit" class="btn btn-success" name="addNew" value="{{trans('site.addNew')}}">
            </div>

        </form>
    </div>
    <div class="col-md-6">
        <div class="box-body">
            <label for="table"> {{trans('site.last_added')}} </label>
            @if($cities->count() > 0)

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> @lang('site.name_ar') </th>
                        <th> @lang('site.name_en') </th>
                        <th> @lang('site.action') </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($cities as $index=>$city )
                    <tr>
                        <td> {{ $index+1 }} </td>
                        <td> {{ $city->name_ar }} </td>
                        <td> {{ $city->name_en }} </td>
                        <td>
                            <a href=" {{route('city.edit',$city->id)}} " class="btn btn-info"> <i class="fas fa-edit"></i> @lang('site.edit') </a>
                            <form action=" {{ route('city.destroy',$city->id) }} " method="POST" style="display:inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else

            <h2> @lang('site.no_data_found') </h2>

            @endif

        </div>
    </div>
</div>





@endsection