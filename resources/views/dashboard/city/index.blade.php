@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.city')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('city.index') }}">@lang('site.city')</a></li>
</ol>
@endsection
@include('dashboard.layouts.includes.errors')
<div class="box-body">
    @if($cities->count() > 0)

    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th> # </th>
                <th> @lang('site.name_ar') </th>
                <th> @lang('site.name_en') </th>
                @if(auth()->user()->hasRole('super_admin'))
                <th> @lang('site.action') </th>
                @endif
            </tr>
        </thead>
        <tbody>
            @foreach($cities as $index=>$city )
            <tr>
                <td> {{ $index+1 }} </td>
                <td> {{ $city->name_ar }} </td>
                <td> {{ $city->name_en }} </td>
                {{--@if(auth()->user()->hasRole('super_admin'))--}}
                <td>
                    <a href=" {{route('city.edit',$city->id)}} " class="btn btn-info"> <i class="fas fa-edit"></i> @lang('site.edit') </a>
                    <form action=" {{ route('city.destroy',$city->id) }} " method="POST" style="display:inline-block">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                    </form>
                </td>
                {{--@endif--}}
            </tr>
            @endforeach
        </tbody>
    </table>
    @else

    <h2> @lang('site.no_data_found') </h2>

    @endif

</div>

@endsection