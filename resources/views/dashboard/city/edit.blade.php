@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.dashboard')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('city.index') }}">@lang('site.cities')</a></li>
    <li class="active"><a href="{{ route('city.create') }}">@lang('site.create_city')</a></li>
</ol>
@endsection

@include('dashboard.layouts.includes.errors')

<form action=" {{ route('city.update',$city) }} " method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <input type="text" value="{{ $city->name_ar }}" class="form-control" name="name_ar" placeholder="@lang('site.name_ar')">
    </div>

    <div class="form-group">
        <input type="text" value="{{ $city->name_en }}" class="form-control" name="name_en" placeholder="@lang('site.name_en')">
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> @lang('site.add')</button>
    </div>

</form>

@endsection