 @extends('dashboard.layouts.master')
 @section('content')

 @section('header')
 <h1>
     @lang('site.addresses')
     <small>@lang('site.create') </small>
 </h1>
 <ol class="breadcrumb">

     <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
     <li><a href="{{ route('address.index') }}">@lang('site.addresses')</a></li>
     <li class="active"><a href="{{ route('address.index') }}">@lang('site.create')</a></li>
 </ol>
 @endsection
 @include('dashboard.layouts.includes.errors')


 <form action="{{ route('address.update',$address) }}" method="POST">
     @csrf
     @method('PUT')
     <div class="box-body">
         <div class="form-group">

             <textarea name="address" id="" cols="30" rows="4" class="form-control" placeholder="@lang('site.address')"> {{$address->address}} </textarea>
         </div>

         <div class=" form-group">
             <select name="type_of_address" id="" class="form-control">
                 <option> {{$address->type_of_address}} </option>
                 <option value="home"> {{trans('site.home')}} </option>
                 <option value="work"> {{trans('site.work')}} </option>
             </select>
         </div>

         <div class="form-group">
             <input type="text" class="form-control" name="depart_number" value="{{$address->depart_number}}" placeholder="@lang('site.depart_number') ">
         </div>

         <div class="form-group">
             <input type="text" class="form-control" name="mobile" value="{{$address->mobile}}" placeholder="@lang('site.mobile') ">
         </div>

         <div class="form-group">
             <input type="text" class="form-control" name="area" value="{{$address->area}}" placeholder="@lang('site.area') ">
         </div>

         <div class="form-group">
             <input type="text" class="form-control" name="latitude" value="{{$address->latitude}}" placeholder="@lang('site.latitude') ">
         </div>

         <div class="form-group">
             <input type="text" class="form-control" name="longitude" value="{{$address->longitude}}" placeholder="@lang('site.longitude') ">
         </div>


         <div class="form-group">
             <input type="text" class="form-control" name="nearest_figuer" value="{{$address->nearest_figuer}}" placeholder="@lang('site.nearest_figuer') ">
         </div>


         <div class="form-group">
             <textarea name="description_ar" id="" class="form-control" cols="30" rows="4" placeholder="@lang('site.description_ar')">{{$address->description_ar}}</textarea>
         </div>

         <div class="form-group">
             <textarea name="description_en" id="" class="form-control" cols="30" rows="4" placeholder="@lang('site.description_en')">{{$address->description_en}}</textarea>
         </div>



         <div class="form-group">
             <select name="city_id" id="" class="form-control">
                 <option value="{{$address->city->id}}"> {{$address->city->$name ?? ''}} </option>
                 @foreach($cities as $city)
                 <option value="{{$city->id}}"> {{$city->$name ?? ''}} </option>
                 @endforeach
             </select>
         </div>



     </div>

     <div class="box-footer">
         <button type="submit" class="btn btn-success">{{trans('site.edit')}}</button>
     </div>
 </form>


 @endsection