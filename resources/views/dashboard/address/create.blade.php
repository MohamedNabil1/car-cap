 @extends('dashboard.layouts.master')
 @section('content')

 @section('header')
 <h1>
     @lang('site.addresses')
     <small>@lang('site.create') </small>
 </h1>
 <ol class="breadcrumb">

     <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
     <li><a href="{{ route('address.index') }}">@lang('site.addresses')</a></li>
     <li class="active"><a href="{{ route('address.index') }}">@lang('site.create')</a></li>
 </ol>
 @endsection
 @include('dashboard.layouts.includes.errors')
 <div class="row">
     <div class="col-md-6">
         <form action="{{ route('address.store') }}" method="POST">
             @csrf
             <div class="box-body">
                 <div class="form-group">

                     <textarea name="address" id="" cols="30" rows="4" class="form-control" placeholder="@lang('site.address')"></textarea>
                 </div>

                 <div class=" form-group">
                     <select name="type_of_address" id="" class="form-control">
                         <option value="home"> {{trans('site.home')}} </option>
                         <option value="work"> {{trans('site.work')}} </option>
                     </select>
                 </div>

                 <div class="form-group">
                     <input type="text" class="form-control" name="depart_number" placeholder="@lang('site.depart_number') ">
                 </div>

                 <div class="form-group">
                     <input type="text" class="form-control" name="mobile" placeholder="@lang('site.mobile') ">
                 </div>

                 <div class="form-group">
                     <input type="text" class="form-control" name="area" placeholder="@lang('site.area') ">
                 </div>

                 <div class="form-group">
                     <input type="text" class="form-control" name="latitude" placeholder="@lang('site.latitude') ">
                 </div>

                 <div class="form-group">
                     <input type="text" class="form-control" name="longitude" placeholder="@lang('site.longitude') ">
                 </div>


                 <div class="form-group">
                     <input type="text" class="form-control" name="nearest_figuer" placeholder="@lang('site.nearest_figuer') ">
                 </div>


                 <div class="form-group">
                     <textarea name="description_ar" id="" class="form-control" cols="30" rows="4" placeholder="@lang('site.description_ar')"></textarea>
                 </div>

                 <div class="form-group">
                     <textarea name="description_en" id="" class="form-control" cols="30" rows="4" placeholder="@lang('site.description_en')"></textarea>
                 </div>



                 <div class="form-group">
                     <select name="city_id" id="" class="form-control">
                         @foreach($cities as $city)
                         <option value="{{$city->id}}"> {{$city->$name ?? ''}} </option>
                         @endforeach
                     </select>
                 </div>



             </div>

             <div class="box-footer">
                 <input type="submit" class="btn btn-primary" name="add" value="{{trans('site.add')}}">
                 <input type="submit" class="btn btn-success" name="addNew" value="{{trans('site.addNew')}}">
             </div>
         </form>
     </div>
     <div class="col-md-6">
         <label for="table"> {{@trans('site.last_added')}} </label>
         <div class="box-body">
             @if($addresses->count() > 0)

             <table class="table table-bordered table-hover">
                 <thead>
                     <tr>
                         <th> # </th>
                         <th> @lang('site.address') </th>
                         <th> @lang('site.city') </th>
                         <th> @lang('site.type_of_address') </th>
                         <th> @lang('site.depart_number') </th>
                         <th> @lang('site.mobile') </th>
                         <th> @lang('site.area') </th>
                         <th> @lang('site.nearest_figuer') </th>
                         <th> @lang('site.description') </th>
                         <th> @lang('site.action') </th>
                     </tr>
                 </thead>
                 <tbody>
                     @foreach($addresses as $index=>$address )
                     <tr>
                         <td> {{ $index+1 }} </td>
                         <td> {{$address->address}}</td>
                         <td> {{ $address->city->$name ?? '' }} </td>
                         <td> {{$address->type_of_address}}</td>
                         <td> {{ $address->depart_number }} </td>
                         <td> {{$address->mobile}} </td>
                         <td> {{$address->area}} </td>
                         <td> {{ $address->nearest_figuer }} </td>
                         <td> {{ $address->$description }} </td>
                         <td>
                             <a href=" {{route('address.edit',$address->id)}} " class="btn btn-info"> <i class="fas fa-edit"></i> @lang('site.edit') </a>
                             <a href=" {{route('address.show',$address)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> @lang('site.see_more') </a>
                             <form action=" {{ route('address.destroy',$address->id) }} " method="POST" style="display:inline-block">
                                 @csrf
                                 @method('DELETE')
                                 <button type="submit" class="btn btn-danger delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                             </form>
                         </td>
                     </tr>
                     @endforeach
                 </tbody>
             </table>
             @else

             <h2> @lang('site.no_data_found') </h2>

             @endif

         </div>
     </div>
 </div>







 @endsection