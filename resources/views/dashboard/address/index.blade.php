@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.addresses')
    <small>@lang('site.preview_all') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('address.index') }}">@lang('site.addresses')</a></li>
</ol>
@endsection
<div class="box-header with-border">
    <h3 class="box-title mb-3"> @lang('site.addresses') </h3> <small>{{$addresses->count() != null ? $addresses->count() : 0 }}</small>
    <form action="{{ route('address.index') }}" method="get">
        <div class="row">
            <div class="col-md-4">
                <input type="text" class="form-control" name="search" placeholder="search" value="{{request()->search}}">
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> Search</button>
                <a href="{{ route('address.create') }}" class="btn btn-info"> {{trans('site.add')}} </a>
            </div>
        </div>
    </form>
</div>

@include('dashboard.layouts.includes.errors')

<div class="box-body">
    @if($addresses->count() > 0)

    <table class="table table-bordered table-hover">
        <thead>

            <tr>
                <th> # </th>
                <th> @lang('site.address') </th>
                <th> @lang('site.city') </th>
                <th> @lang('site.type_of_address') </th>
                <th> @lang('site.depart_number') </th>
                <th> @lang('site.mobile') </th>
                <th> @lang('site.area') </th>
                <th> @lang('site.nearest_figuer') </th>
                <th> @lang('site.description') </th>
                <th> @lang('site.action') </th>
            </tr>

        </thead>
        <tbody>
            @foreach($addresses as $index=>$address )
            <tr>
                <td> {{ $index+1 }} </td>
                <td> {{$address->address}}</td>
                <td> {{ $address->city->$name ?? '' }} </td>
                <td> {{$address->type_of_address}}</td>
                <td> {{ $address->depart_number }} </td>
                <td> {{$address->mobile}} </td>
                <td> {{$address->area}} </td>
                <td> {{ $address->nearest_figuer }} </td>
                <td> {{ $address->$description }} </td>
                <td>
                    <a href=" {{route('address.edit',$address->id)}} " class="btn btn-info"> <i class="fas fa-edit"></i> @lang('site.edit') </a>
                    <a href=" {{route('address.show',$address)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> @lang('site.see_more') </a>
                    <form action=" {{ route('address.destroy',$address->id) }} " method="POST" style="display:inline-block">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else

    <h2> @lang('site.no_data_found') </h2>

    @endif

</div>





@endsection