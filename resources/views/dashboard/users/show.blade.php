@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.dashboard')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

    </ol>
@endsection

<!-- Content Wrapper. Contains page content -->

<section class="content-header">
    @include('dashboard.layouts.includes.errors')

    <h1>
        {{$user->full_name}}
    </h1>
    <ol class="breadcrumb">
        <li class="#"><a href="{{ route('dashboard.index') }}"><h4><i
                            class="fa fa-dashboard"></i> @lang('site.dashboard')</h4></a></li>
        <li class="active"><a href="{{ route('users.index') }}"><h4>@lang('site.users')</h4></a></li>
    </ol>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-3">
            <!------------------------------------------------------- start about ---------------------------------------->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{$user->ImagePath}}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center"> {{$user->full_name}} </h3>

                    <p class="text-muted text-center">{{$user->type}} </p>

                    <ul class="list-group list-group-unbordered">

                        <li class="list-group-item">
                            <b>@lang('site.number_of_orders')</b> <a class="pull-right">{{$user_orders}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>@lang('site.user_type')</b> <a class="pull-right">{{ $user->type }}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <h3 class="profile-username text-center"> ارسال اشعار للعميل </h3>
                    <form class="form-horizontal" action="{{route('send_notify')}}"
                          method="post" enctype="multipart/form-data" style="border:1px solid black;padding:20px 30px">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $user->id }}"/>
                        <label> عنوان الاشعار </label>
                        <input type="text" required class="form-control" name="title"/>
                        <br>
                        <label> نص الاشعار </label>
                        <textarea class="form-control" name="value_ar" required></textarea>
                        <br>
                        <center>
                            <button type="submit"
                                    class="btn btn-primary btn-block"> ارسال الاشعار
                            </button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
        <!--------------------------------------------- end About -------------------------------------->

        <!------------------------------------ Tabs -------------------------------->
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#activity" data-toggle="tab">@lang('site.main_info')</a></li>
                        <li><a href="#address" data-toggle="tab">@lang('site.addresses')</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="activity">
                            <div class="box-body">
                                @if($user->count() > 0)
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th> @lang('site.full_name') </th>
                                            <th> @lang('site.email') </th>
                                            <th> @lang('site.mobile') </th>
                                            <th> @lang('site.type') </th>
                                            <th> @lang('site.city') </th>
                                            <th> @lang('site.image') </th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr>
                                            <td> {{ $user->full_name }} </td>
                                            <td> {{ $user->email }} </td>
                                            <td> {{ $user->mobile }} </td>
                                            <td> {{ $user->type }} </td>
                                            @if($user->city_id != null)
                                                <td> {{ $user->city->name_ar }}-{{ $user->city->name_en }} </td>
                                            @else
                                                <td> Admin City</td>
                                            @endif
                                            <td><img src="{{ $user->ImagePath }}" style="width: 80px ; height:80px">
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                @else
                                    <h2> @lang('site.no_data_found') </h2>
                                @endif
                            </div>
                        </div>

                        <!------------------------------------ Advertises Tab -------------------------------->
                        <div class="tab-pane" id="timeline">
                        </div>
                        <!------------------------------------End Advertises Tab -------------------------------->


                        <!------------------------------------ Address  Tab -------------------------------->
                        <div class="tab-pane" id="address">
                            <div class="box-body">
                                @if($addresses->count() > 0)
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th> #</th>
                                            <th> @lang('site.address') </th>
                                            {{--<th> @lang('site.city') </th>--}}
                                            <th> @lang('site.type_of_address') </th>
                                            <th> @lang('site.depart_number') </th>
                                            <th> @lang('site.mobile') </th>
                                            <th> @lang('site.area') </th>
                                            <th> @lang('site.nearest_figuer') </th>
                                            <th> @lang('site.description') </th>
                                            <th> @lang('site.action') </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($addresses as $index=>$address )
                                            <tr>
                                                <td> {{ $index+1 }} </td>
                                                <td> {{$address->address}}</td>
                                                <td> {{$address->type_of_address}}</td>
                                                <td> {{ $address->depart_number }} </td>
                                                <td> {{$address->mobile}} </td>
                                                <td> {{$address->area}} </td>
                                                <td> {{ $address->nearest_figuer }} </td>
                                                <td> {{ $address->$description }} </td>
                                                <td>
                                                    <form action=" {{ route('address.destroy',$address->id) }} "
                                                          method="POST" style="display:inline-block">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" class="btn btn-danger delete"><i
                                                                    class="fa fa-trash"></i> @lang('site.delete')
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h2> @lang('site.no_data_found') </h2>
                                @endif
                            </div>
                        </div>
                        <!------------------------------------End Address  Tab -------------------------------->
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>
<!-- /.content -->

@endsection