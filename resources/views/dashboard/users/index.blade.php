@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.dashboard')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('users.index') }}">@lang('site.users')</a></li>
</ol>
@endsection


<div class="box box-primary">
    @include('dashboard.layouts.includes.errors')

    <div class="box-header with-border">
        <h3 class="box-title"> @lang('users') </h3> <small>{{$users->total() != null ? $users->total() : 0 }}</small>
        <form action="{{ route('users.index') }}" method="get">

            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="search" class="form-control" placeholder="@lang('site.search')" value="{{request()->search}}">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> @lang('site.search') </button>
                    <a href="{{ route('users.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> @lang('site.create') </a>
                </div>
            </div>
        </form>

    </div>

    <div class="box-body">
        @if($users->count() > 0)

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th> # </th>
                    <th> @lang('site.full_name') </th>
                    <th> @lang('site.email') </th>
                    <th> @lang('site.type') </th>
                    <th> @lang('site.city') </th>
                    <th> @lang('site.image') </th>
                    <th> @lang('site.action') </th>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $index=>$user )
                <tr>
                    <td> {{ $index+1 }} </td>
                    <td> {{ $user->full_name }} </td>
                    <td> {{ $user->email }} </td>
                    <td> {{ $user->type }} </td>
                    @if($user->city_id != null)
                    <td> {{ $user->city->name_ar }}-{{ $user->city->name_en }} </td>
                    @else
                    <td> Admin City </td>
                    @endif
                    <td><img src="{{ $user->ImagePath }}" style="width: 80px ; height:80px"></td>
                    <td>
                        <a href=" {{route('users.edit',$user->id)}} " class="btn btn-info btn-sm"> <i class="fas fa-edit"></i> @lang('site.edit') </a>
                        <a href=" {{route('users.show',$user->id)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> @lang('site.see_more') </a>
                        <form action=" {{ route('users.destroy',$user->id) }} " method="POST" style="display:inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{$users->appends(request()->query())->links()}}
        @else
        <h2> @lang('site.no_data_found') </h2>
        @endif

    </div>
</div>
@endsection