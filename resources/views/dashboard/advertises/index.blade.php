@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.advertises')
    <small>@lang('site.preview_all') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('advertises.index') }}">@lang('site.advertises')</a></li>
</ol>
@endsection
<div class="box-header with-border">
    <h3 class="box-title mb-3"> @lang('site.advertises') </h3>  :
    <small>{{$advertises->count() != null ? $advertises->count() : 0 }}</small>
    <form action="{{ route('advertises.index') }}" method="get">
        <div class="row">
            <div class="col-md-4">
                <input type="text" class="form-control" name="search" placeholder="search" value="{{request()->search}}">
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> Search</button>
                <a href="{{ route('advertises.create') }}" class="btn btn-info"> {{trans('site.add')}} </a>
            </div>
        </div>
    </form>
</div>

@include('dashboard.layouts.includes.errors')

<div class="box-body">
    @if($advertises->count() > 0)

    <table class="table table-bordered table-hover">
        <thead>
            <tr>
                <th> # </th>
                <th> @lang('site.title_ar') </th>
                <th> @lang('site.title_en') </th>
                <th> @lang('site.body_ar') </th>
                <th> @lang('site.body_en') </th>
                <th> @lang('site.image') </th>
                <th> @lang('site.action') </th>

            </tr>
        </thead>
        <tbody>
            @foreach($advertises as $index=>$advertise )
            <tr>
                <td> {{ $index+1 }} </td>
                <td> {{ $advertise->title_ar }} </td>
                <td> {{ $advertise->title_en }} </td>
                <td> {{ $advertise->body_ar }} </td>
                <td> {{ $advertise->body_en }} </td>
                <td><img src="{{ $advertise->ImagePath }}" style="width: 80px ; height:80px"></td>
                <td>
                    {{--<a href=" {{route('advertises.edit',$advertise->id)}} " class="btn btn-info btn-sm"> <i class="fas fa-edit"></i> @lang('site.edit') </a>--}}
                    {{--<a href=" {{route('advertises.show',$advertise->id)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> @lang('site.see_more') </a>--}}
                    <form action=" {{ route('advertises.destroy',$advertise->id) }} " method="POST" style="display:inline-block">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    @else

    <h2> @lang('site.no_data_found') </h2>

    @endif

</div>





@endsection