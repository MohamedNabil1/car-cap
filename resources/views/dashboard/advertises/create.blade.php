@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.advertises')
    <small>@lang('site.create') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li><a href="{{ route('advertises.index') }}">@lang('site.advertises')</a></li>
    <li class="active"><a href="{{ route('advertises.index') }}">@lang('site.create')</a></li>
</ol>
@endsection
@include('dashboard.layouts.includes.errors')
<div class="row">
    <div class="col-md-6">
        <form action="{{ route('advertises.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <input type="text" class="form-control" name="title_ar" placeholder="@lang('site.title_ar') ">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="title_en" placeholder="@lang('site.title_en') ">
                </div>

                <div class="form-group">
                    <textarea name="body_ar" class="form-control" cols="30" rows="5" placeholder="{{ trans('site.body_ar') }}">  </textarea>
                </div>

                <div class="form-group">
                    <textarea name="body_en" class="form-control" cols="30" rows="5" placeholder="{{ trans('site.body_en') }}">  </textarea>
                </div>

                <div class="form-group">
                    <label>@lang('site.image')</label>
                    <input type="file" class="form-control image " name="image">
                </div>

                <div class="form-group">
                    <img src=" {{ asset('public/uploads/default.png') }} " width=" 100px " class="thumbnail image-preview">
                </div>
            </div>

            <div class="box-footer">
                <input type="submit" name="add" value="{{trans('site.add')}}">
                <input type="submit" name="addNew" value="{{trans('site.addNew')}}">
            </div>
        </form>
    </div>
    <div class="col-md-6">
        <label for="table"> {{@trans('site.last_added')}} </label>
        <div class="box-body">
            @if($advertises->count() > 0)

            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th> # </th>
                        <th> @lang('site.title_ar') </th>
                        <th> @lang('site.title_en') </th>
                        <th> @lang('site.body_ar') </th>
                        <th> @lang('site.body_en') </th>
                        <th> @lang('site.image') </th>
                        <th> @lang('site.action') </th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($advertises as $index=>$advertise )
                    <tr>
                        <td> {{ $index+1 }} </td>
                        <td> {{ $advertise->title_ar }} </td>
                        <td> {{ $advertise->title_en }} </td>
                        <td> {{ $advertise->body_ar }} </td>
                        <td> {{ $advertise->body_en }} </td>
                        <td><img src="{{ $advertise->ImagePath }}" style="width: 80px ; height:80px"></td>
                        <td>
                            {{--<a href=" {{route('advertises.edit',$advertise->id)}} " class="btn btn-info btn-sm"> <i class="fas fa-edit"></i> @lang('site.edit') </a>--}}
                            {{--<a href=" {{route('advertises.show',$advertise->id)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> @lang('site.see_more') </a>--}}
                            <form action=" {{ route('advertises.destroy',$advertise->id) }} " method="POST" style="display:inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm delete"> <i class="fa fa-trash"></i> @lang('site.delete') </button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @else

            <h2> @lang('site.no_data_found') </h2>

            @endif

        </div>
    </div>
</div>







@endsection