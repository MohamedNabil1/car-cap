@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.dashboard')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

        <li class="#"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
        <li class="active"><a href="{{ route('providers.index') }}">@lang('site.providers')</a></li>
        <li class="active"><a href="{{ route('providers.create') }}">@lang('site.create_providers')</a></li>
    </ol>
@endsection

@include('dashboard.layouts.includes.errors')

<form action=" {{ route('providers.store') }} " method="POST" enctype="multipart/form-data">
    @csrf

    <div class="box-body">
        <div>
            <label for=""> @lang('site.main_info') </label>

            <div class="form-group">

                <input type="text" class="form-control" name="full_name" placeholder="@lang('site.full_name')">
            </div>

            <div class="form-group">

                <input type="email" class="form-control" name="email" placeholder="@lang('site.email')">
            </div>

            <div class="form-group">

                <input type="text" class="form-control" name="mobile" placeholder="@lang('site.mobile')">
            </div>

            <div class="form-group">
                <select name="city_id" id="" class="form-control">
                    <option value="" disabled selected> {{trans('site.cities')}} </option>
                    @foreach($cities as $city)
                        <option value="{{$city->id}}">{{$city->name_ar}}-{{$city->name_en}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="password" class="form-control" name="password" placeholder="@lang('site.password')">
            </div>

            <div class="form-group">

                <input type="password" class="form-control" name="password_confirmation"
                       placeholder="@lang('site.password_confirmation')">

            </div>

            <label for=""> @lang('site.add_address') </label>
            <div class="form-group">
                <input type="text" class="form-control" name="address" placeholder="@lang('site.address')">
            </div>

            <div class="form-group">
                <select name="type_of_address" id="" class="form-control">
                    <option value="" disabled selected>{{trans('site.type_of_address')}}</option>
                    <option value="home"> {{trans('site.home')}} </option>
                    <option value="work"> {{trans('site.work')}} </option>
                </select>

            </div>

            <div class="form-group">

                <input type="text" class="form-control" name="depart_number" placeholder="@lang('site.depart_num')">
            </div>

            <div class="form-group">

                <input type="text" class="form-control" name="description_ar"
                       placeholder="@lang('site.description_ar')">
            </div>
            <div class="form-group">

                <input type="text" class="form-control" name="description_en"
                       placeholder="@lang('site.description_en')">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" name="area" placeholder="@lang('site.area')">
            </div>


            <div class="form-group">
                <label>@lang('site.image')</label>
                <input type="file" class="form-control image " name="image">
            </div>

            <div class="form-group">
                <img src=" {{ asset('public/uploads/default.png') }} " width=" 100px " class="thumbnail image-preview">
            </div>

        </div>
    </div>

    <div class="box-footer">
        <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> {{trans('site.add')}}</button>
    </div>


</form>

@endsection