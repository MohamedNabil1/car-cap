@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.dashboard')
        <small>@lang('site.control_panel') </small>
    </h1>
@endsection

<!-- Content Wrapper. Contains page content -->

<section class="content-header">
    <h1>
        {{$provider->full_name}}
    </h1>
    <ol class="breadcrumb">
        <li class="#"><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i>@lang('site.dashboard')
            </a>
        @if (auth()->user()->hasRole('admin'))
        <li class="active"><a href="{{ route('providers.index') }}">@lang('site.providers')</a></li>
        @endif
    </ol>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-3">
            <!------------------------------------------------------- start about ---------------------------------------->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{$provider->ImagePath}}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center"> {{$provider->full_name}} </h3>

                    <p class="text-muted text-center">{{$provider->type}} </p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>@lang('site.number_of_products')</b> <a
                                    class="pull-right">{{ $provider->product  ? count($provider->product) : 0 }}</a>
                        </li>

                        <li class="list-group-item">
                            <b>@lang('site.number_favorites')</b> <a
                                    class="pull-right">{{ $provider->favorite ? count($provider->favorite) : 0 }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>@lang('site.user_type')</b> <a class="pull-right">{{ $provider->type }}</a>
                        </li>
                    </ul>

                    <a href=" {{ route('products.create') }} " class="btn btn-primary btn-block"><b> <i
                                    class="fa fa-plus"></i> {{ trans('site.add_advertise') }}</b></a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">طلباتي</h3>
                </div>
                <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i> {{ trans('site.avg_rate') }}</strong>

                    <p class="text-muted">
                        <span class="label label-danger">{{ $provider->rates }} </span>
                    </p>
                    <hr>
                    <strong><i class="fa fa-pencil margin-r-5"></i> {{ trans('site.number_penning_orders') }} </strong>
                    <p>
                        <span class="label label-primary">{{ $pending_orders != 0 ? count($pending_orders) : 0 }}</span>
                    </p>
                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> {{ trans('site.number_on_processing_orders') }}
                    </strong>
                    <p>
                        <span class="label label-primary">{{ $on_processing_orders != 0 ? count($on_processing_orders) : 0 }}</span>
                    </p>
                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> {{ trans('site.number_on_shipping_orders') }}
                    </strong>
                    <p>
                        <span class="label label-primary">{{ $on_shipping_orders != 0 ? count($on_shipping_orders) : 0 }}</span>
                    </p>
                    <hr>

                    <strong><i class="fa fa-pencil margin-r-5"></i> {{ trans('site.number_delivered__orders') }}
                    </strong>
                    <p>
                        <span class="label label-primary">{{ $delivered_orders != 0 ? count($delivered_orders) : 0 }}</span>
                    </p>
                    <hr>


                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>
                    <p> {{$provider->description}} </p>
                </div>
            </div>
        </div>

        <!--------------------------------------------- end About -------------------------------------->

        <!------------------------------------ Tabs -------------------------------->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">@lang('site.main_info')</a></li>
                    <li><a href="#timeline" data-toggle="tab">@lang('site.provider_ads')</a></li>
                    <li><a href="#address" data-toggle="tab">@lang('site.addresses')</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="box-body">
                            @if($provider->count() > 0)

                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> @lang('site.full_name') </th>
                                        <th> @lang('site.email') </th>
                                        <th> @lang('site.type') </th>
                                        <th> @lang('site.city') </th>
                                        <th> @lang('site.image') </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td> {{ $provider->full_name }} </td>
                                        <td> {{ $provider->email }} </td>
                                        <td> {{ $provider->type }} </td>
                                        @if($provider->city_id != null)
                                            <td> {{ $provider->city->name_ar }}-{{ $provider->city->name_en }} </td>
                                        @else
                                            <td> provider City</td>
                                        @endif
                                        <td><img src="{{ $provider->ImagePath }}" style="width: 80px ; height:80px">
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif

                        </div>
                    </div>

                    <!------------------------------------ Products Tab -------------------------------->
                    <div class="tab-pane" id="timeline">
                        <div class="box-body">
                            @if($products->count() > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.name_ar') </th>
                                        <th> @lang('site.owner') </th>
                                        <th> @lang('site.description_ar') </th>
                                        <th> @lang('site.price') </th>
                                        <th> @lang('site.category') </th>
                                        <th> @lang('site.model') </th>
                                        <th> @lang('site.production_year') </th>
                                        <th> @lang('site.status') </th>
                                        <th> @lang('site.serial') </th>
                                        <th> @lang('site.views_num') </th>
                                        <th> @lang('site.like_count') </th>
                                        <th> @lang('site.rate_avg') </th>
                                        {{--<th> @lang('site.accepted') </th>--}}
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($products as $index=>$product )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{$product->$name}}</td>
                                            <td> {{ $product->provider->full_name }} </td>
                                            <td> {{$product->$description}}</td>
                                            <td> {{ $product->price }} </td>
                                            <td> {{$product->category->$name}} </td>
                                            <td> {{ $product->model }} </td>
                                            <td> {{ $product->production_year }} </td>
                                            <td> {{ $product->status }} </td>
                                            <td> {{ $product->serial }} </td>
                                            <td> {{ $product->views_num }} </td>
                                            <td> {{ $product->like_count }} </td>
                                            <td> {{ $product->rate_avg }} </td>
                                            {{--<td> {{ $product->admin_approve == 0 ? trans('site.not_approved') : trans('site.approved') }} </td>--}}
                                            <td>
                                                <a href=" {{route('products.edit',$product->id)}} "
                                                   class="btn btn-info"> <i class="fas fa-edit"></i> @lang('site.edit')
                                                </a>
                                                <a href=" {{route('products.show',$product)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.see_more') </a>
                                                <form action=" {{ route('products.destroy',$product->id) }} "
                                                      method="POST" style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                                {!! $products->links() !!}


                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End Products Tab -------------------------------->


                    <!------------------------------------ Address  Tab -------------------------------->
                    <div class="tab-pane" id="address">
                        <div class="box-body">
                            @if($addresses->count() > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.address') </th>
                                        <th> @lang('site.city') </th>
                                        <th> @lang('site.type_of_address') </th>
                                        <th> @lang('site.depart_number') </th>
                                        <th> @lang('site.mobile') </th>
                                        <th> @lang('site.area') </th>
                                        <th> @lang('site.nearest_figuer') </th>
                                        <th> @lang('site.description') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($addresses as $index=>$address )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{$address->address}}</td>
                                            <td> {{ $address->city->$name ?? '' }} </td>
                                            <td> {{$address->type_of_address}}</td>
                                            <td> {{ $address->depart_number }} </td>
                                            <td> {{$address->mobile}} </td>
                                            <td> {{$address->area}} </td>
                                            <td> {{ $address->nearest_figuer }} </td>
                                            <td> {{ $address->$description }} </td>
                                            <td>
                                                <a href=" {{route('address.edit',$address->id)}} " class="btn btn-info">
                                                    <i class="fas fa-edit"></i> @lang('site.edit') </a>
                                                <a href=" {{route('address.create')}} " class="btn btn-success"> <i
                                                            class="fas fa-plus"></i> @lang('site.add') </a>
                                                <form action=" {{ route('address.destroy',$address->id) }} "
                                                      method="POST" style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End Address  Tab -------------------------------->
                </div>
            </div>
        </div>


        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#pending_orders" data-toggle="tab">@lang('site.pending_orders')</a></li>
                    <li><a href="#on_processing" data-toggle="tab">@lang('site.on_processing_orders')</a></li>
                    <li><a href="#on_shipping" data-toggle="tab">@lang('site.on_shipping_orders')</a></li>
                    <li><a href="#delivered" data-toggle="tab">@lang('site.delivered_orders')</a></li>
                </ul>
                <div class="tab-content">

                    <!------------------------------------  Pending -------------------------------->
                    <div class="active tab-pane" id="pending_orders">
                        <div class="box-body">
                            @if(count($pending_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pending_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('process.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.process') </a>
                                                <a href=" {{route('ship.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.ship') </a>
                                                <a href=" {{route('deliver.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.delivered') </a>
                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>

                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End  Pending -------------------------------->


                    <!------------------------------------ On processing -------------------------------->
                    <div class="tab-pane" id="on_processing">
                        <div class="box-body">
                            @if(count($on_processing_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($on_processing_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('ship.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.ship') </a>
                                                <a href=" {{route('deliver.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.delivered') </a>

                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>
                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End On processing -------------------------------->


                    <!------------------------------------ On shipping -------------------------------->
                    <div class="tab-pane" id="on_shipping">
                        <div class="box-body">
                            @if(count($on_shipping_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($on_shipping_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('deliver.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.delivered') </a>
                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>
                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End On shipping -------------------------------->


                    <!------------------------------------  Delivered  (Done) -------------------------------->
                    <div class="tab-pane" id="delivered">
                        <div class="box-body">
                            @if(count($delivered_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        {{--<th> @lang('site.order_status') </th>--}}
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($delivered_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            {{--<td> {{$order->status}}</td>--}}
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>
                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------ Delivered  -------------------------------->


                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->

@endsection