@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.dashboard')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

        <li class="#"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
        <li class="active"><a href="{{ route('providers.index') }}">@lang('site.providers')</a></li>
    </ol>
@endsection


<div class="box box-primary">
    @include('dashboard.layouts.includes.errors')

    <div class="box-header with-border">
        <h3 class="box-title"> @lang('providers') </h3>
        <small>{{$providers->total() != null ? $providers->total() : 0 }}</small>
        <form action="{{ route('providers.index') }}" method="get">

            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="search" class="form-control" placeholder="@lang('site.search')"
                           value="{{request()->search}}">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary btn-sm"><i
                                class="fa fa-search"></i> @lang('site.search') </button>
                    <a href="{{ route('providers.create') }}" class="btn btn-info btn-sm"><i
                                class="fa fa-plus"></i> @lang('site.create') </a>
                </div>
            </div>
        </form>

    </div>

    <div class="box-body">
        @if($providers->count() > 0)

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th> #</th>
                    <th> @lang('site.full_name') </th>
                    <th> @lang('site.email') </th>
                    <th> @lang('site.type') </th>
                    <th> @lang('site.city') </th>
                    <th> @lang('site.image') </th>
                    <th> @lang('site.action') </th>
                </tr>
                </thead>
                <tbody>
                @foreach($providers as $index=>$provider )
                    <tr>
                        <td> {{ $index+1 }} </td>
                        <td> {{ $provider->full_name }} </td>
                        <td> {{ $provider->email }} </td>
                        <td> {{ $provider->type }} </td>
                        @if($provider->city_id != null)
                            <td> {{ $provider->city->name_ar }}-{{ $provider->city->name_en }} </td>
                        @else
                            <td> Admin City</td>
                        @endif
                        <td><img src="{{ $provider->ImagePath }}" style="width: 80px ; height:80px"></td>
                        <td>
                            <a href=" {{route('providers.edit',$provider->id)}} " class="btn btn-info btn-sm"> <i
                                        class="fas fa-edit"></i> @lang('site.edit') </a>
                            <a href=" {{route('providers.show',$provider->id)}} " class="btn btn-warning btn-sm"> <i
                                        class="fas fa-eye"></i> @lang('site.see_more') </a>
                            <form action=" {{ route('providers.destroy',$provider->id) }} " method="POST"
                                  style="display:inline-block">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm delete"><i class="fa fa-trash"></i> @lang('site.delete') </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{$providers->appends(request()->query())->links()}}
        @else

            <h2> @lang('site.no_data_found') </h2>

        @endif

    </div>
</div>
@endsection