@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.products')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('products.index') }}">@lang('site.products')</a></li>
</ol>
@endsection


@include('dashboard.layouts.includes.errors')
<form action="{{ route('products.update',$product) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="box-body">
        <div class="form-group">
            <input type="text" class="form-control " value="{{$product->name_ar}}" name="name_ar" placeholder="@lang('site.name_ar') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$product->name_en}}" name="name_en" placeholder="@lang('site.name_en') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="company_name" value="{{$product->company_name}}"placeholder="@lang('site.company_name') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$product->description_ar}}" name="description_ar" placeholder="@lang('site.description_ar') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$product->description_en}}" name="description_en" placeholder="@lang('site.description_en') ">
        </div>
        <div class="form-group">
            <select name="status" id="" class="form-control">
                <option value="" disabled> {{trans('site.status')}} </option>
                <option value="new" @if ($product->status == 'new') selected @endif > {{trans('site.new')}} </option>
                <option value="used" @if ($product->status == 'used') selected @endif > {{trans('site.used')}} </option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$product->model}}" name="model" placeholder="@lang('site.model') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" value="{{$product->production_year}}" name="production_year" placeholder="@lang('site.production_year') ">
        </div>
        <div class="form-group">
            <input type="number" class="form-control" value="{{$product->price}}" name="price" placeholder="@lang('site.price') ">
        </div>
        <div>
            <select name="category_id" id="" class="form-control">
                <option value="" class="form-control" disabled> {{trans('site.categories')}} </option>
                @foreach($categories as $category)
                <option value="{{$category->id}}" class="form-control" @if($product->category->id == $category->id) selected @endif> {{\Illuminate\Support\Facades\App::getLocale() == 'ar' ? $category->name_ar : $category->name_en}} </option>
                @endforeach
            </select>
        </div> <br>
        <div>
            <select name="city_id" id="" class="form-control">
                <option value="" class="form-control" disabled> {{trans('site.cities')}} {{\Illuminate\Support\Facades\App::getLocale() == 'ar' ? $product->city->name_ar : $product->city->name_en}} </option>
                @foreach($cities as $city)
                <option value="{{$city->id}}" class="form-control" @if($product->city->id == $city->id) selected @endif > {{\Illuminate\Support\Facades\App::getLocale() == 'ar' ? $city->name_ar : $city->name_en}} </option>
                @endforeach
            </select>
        </div> <br>
        <div class="form-group">
            <input type="file" class="form-control image " name="image">
        </div>

        <div class="form-group">
            <img src=" {{ $product->ImagePath }} " width=" 100px " class="thumbnail image-preview">
        </div>
    </div>

    <div class="box-footer">
        <button class="btn btn-success" type="submit"> <i class="fa fa-edit"></i> {{trans('site.edit')}}</button>
    </div>
</form>




@endsection