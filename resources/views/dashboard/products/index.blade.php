@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.products')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('products.index') }}">@lang('site.products')</a></li>
</ol>
@endsection

<div class="box box-primary">
    @include('dashboard.layouts.includes.errors')

    <div class="box-header with-border">
        <h3 class="box-title"> @lang('site.products') </h3> <small>{{$products ? $products->count() : 0 }}</small>
        <form action="{{ route('products.index') }}" method="get">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" name="search" class="form-control" placeholder="@lang('site.search_with_name')" value="{{request()->search}}">
                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-search"></i> @lang('site.search_with_name') </button>
                    <a href="{{ route('products.create') }}" class="btn btn-info btn-sm"><i class="fa fa-plus"></i> @lang('site.create') </a>
                </div>
            </div>
        </form>

    </div>

    <div class="box-body">
        @if($products)

        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th> # </th>
                    <th> @lang('site.product_name_ar') </th>
                    <th> @lang('site.company_name') </th>
                    <th> @lang('site.owner') </th>
                    <th> @lang('site.description_ar') </th>
                    <th> @lang('site.price') </th>
                    <th> @lang('site.category') </th>
                    {{--<th> @lang('site.subcategories') </th>--}}
                    <th> @lang('site.model') </th>
                    <th> @lang('site.production_year') </th>
                    <th> @lang('site.status') </th>
                    {{--<th> @lang('site.serial') </th>--}}
                    {{--<th> @lang('site.views_num') </th>--}}
                    {{--<th> @lang('site.like_count') </th>--}}
                    {{--<th> @lang('site.rate_avg') </th>--}}
                    {{--<th> @lang('site.accepted') </th>--}}
                    <th> @lang('site.action') </th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $index=>$product )
                <tr>
                    <td> {{ $index+1 }} </td>
                    <td> {{$product->$name}}</td>
                    <td> {{$product->company_name}}</td>
                    <td> <a href="{{route('providers.show',$product->provider)}}"> {{ $product->provider->full_name }}</a> </td>
                    <td> {{$product->$description}}</td>
                    <td> {{ $product->price }} </td>
                    <td> {{$product->category->$name ?? ''}} </td>
                    {{--<td> {{$product->subcategory->$name ?? ''}} </td>--}}
                    <td> {{ $product->model }} </td>
                    <td> {{ $product->production_year }} </td>
                    <td> {{ $product->status }} </td>
                    {{--<td> {{ $product->serial }} </td>--}}
                    {{--<td> {{ $product->views_num }} </td>--}}
                    {{--<td> {{ $product->like_count }} </td>--}}
                    {{--<td> {{ $product->rate_avg }} </td>--}}
                    {{--<td> {{ $product->admin_approve == 0 ? trans('site.not_approved') : trans('site.approved') }} </td>--}}
                    {{--@if(auth()->user()->id == $product->user_id)--}}
                    <td>
                        <a href=" {{route('products.edit',$product->id)}} " class="btn btn-info btn-sm"> <i class="fas fa-edit"></i> </a>
                        <a href=" {{route('products.show',$product)}} " class="btn btn-warning btn-sm"> <i class="fas fa-eye"></i> </a>
                        <form action=" {{ route('products.destroy',$product->id) }} " method="POST" style="display:inline-block">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger delete btn-sm"> <i class="fa fa-trash"></i> </button>
                        </form>
                    </td>
                    {{--@endif--}}
                </tr>
                @endforeach
            </tbody>
        </table>
            {{--{{$products->appends(request()->query())->links()}}--}}

        @else

        <h2> @lang('site.no_data_found') </h2>

        @endif

    </div>
</div>




@endsection