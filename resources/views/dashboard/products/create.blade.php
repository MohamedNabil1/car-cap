@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.products')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

        <li class="#"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
        <li class="active"><a href="{{ route('products.index') }}">@lang('site.products')</a></li>
    </ol>
@endsection
@include('dashboard.layouts.includes.errors')
<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="box-body">
        <div class="form-group">
            <input type="text" class="form-control " name="name_ar" placeholder="@lang('site.name_ar') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="name_en" placeholder="@lang('site.name_en') ">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" name="description_ar" placeholder="@lang('site.description_ar') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="description_en" placeholder="@lang('site.description_en') ">
        </div>

        <div class="form-group">
            <select name="city_id" id="" class="form-control">
                <option value="" class="form-control" selected disabled> {{trans('site.cities')}} </option>
                @foreach($cities as $city)
                    <option value="{{$city->id}}" class="form-control"> {{ $city->$name}} </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <select name="category_id" id="category_id" class="form-control">
                <option value="" class="form-control" selected> {{trans('site.categories')}} </option>
                @foreach($categories as $category)
                    <option value="{{$category->id}}" class="form-control"> {{$category->$name}} </option>
                @endforeach
            </select>
        </div>

        @if (auth()->user()->hasRole('admin'))
            <div class="form-group">
                <select name="user_id" id="user_id" class="form-control">
                    <option value="" class="form-control" disabled selected> {{trans('site.providers')}} </option>
                    @foreach($providers as $provider)
                        <option value="{{$provider->id}}" class="form-control"> {{$provider->full_name}} </option>
                    @endforeach
                </select>
            </div>
        @endif

        <div class="form-group">
            <input type="text" class="form-control" name="company_name" placeholder="@lang('site.company_name') ">
        </div>
        <div class="form-group">
            <select name="status" id="" class="form-control">
                <option value=""> {{trans('site.status')}} </option>
                <option value="new"> {{trans('site.new')}} </option>
                <option value="used"> {{trans('site.used')}} </option>
            </select>
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="model" placeholder="@lang('site.model') ">
        </div>
        <div class="form-group">
            <input type="text" class="form-control" name="production_year" placeholder="@lang('site.production_year') ">
        </div>
        <div class="form-group">
            <input type="number" class="form-control" name="price" placeholder="@lang('site.price') ">
        </div>
        <div class="form-group">
            <input type="number" class="form-control" name="stock_count" placeholder="@lang('site.stock_count') ">
        </div>


        <div class="form-group">
            <input type="file" class="form-control " name="images[]" multiple="multiple"/>
        </div>

        <div class="form-group">
            <img src=" {{ asset('public/uploads/default.png') }} " width=" 100px " class="thumbnail image-preview">
        </div>
    </div>

    <div class="box-footer">
        <input type="submit" class="btn btn-primary" name="add" value="{{trans('site.add')}}">
        <input type="submit" class="btn btn-success" name="addNew" value="{{trans('site.addNew')}}">
    </div>
</form>


@endsection