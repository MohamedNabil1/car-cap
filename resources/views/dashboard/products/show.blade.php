@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.product_details')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

        <li class="#"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
        <li class="active"><a href="{{ route('products.index') }}">@lang('site.products')</a></li>
    </ol>
@endsection

<!-- Content Wrapper. Contains page content -->

<section class="content-header">
    <h1>

        {{\Illuminate\Support\Facades\App::getLocale() == "ar" ? $product->name_ar :$product->name_en}}
    </h1>

</section>

<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!------------------------------------------------------- start about ---------------------------------------->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-products-img img-responsive img-circle" src="{{$product->ImagePath}}"
                         alt="products profile picture">

                    <h3 class="profile-productsname text-center"> {{\Illuminate\Support\Facades\App::getLocale() == "ar" ? $product->name_ar :$product->name_en}} </h3>

                    <p class="text-muted text-center">{{ $product->provider->full_name }} </p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>@lang('site.status')</b> <a class="pull-right">{{$product->status}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>@lang('site.price')</b> <a class="pull-right">{{$product->price}}</a>
                        </li>
                    </ul>

                    <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                </div>
            </div>
            <div class="box box-primary">

            </div>
        </div>

        <!--------------------------------------------- end About -------------------------------------->

        <!------------------------------------ Tabs -------------------------------->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#activity" data-toggle="tab">@lang('site.main_info')</a></li>
                    <li><a href="#timeline" data-toggle="tab">@lang('site.owner')</a></li>
                    <li><a href="#settings" data-toggle="tab">@lang('site.statestic')</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                        <div class="box-body">
                            @if($product->count() > 0)

                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> @lang('site.name_ar') </th>
                                        <th> @lang('site.owner') </th>
                                        <th> @lang('site.description_ar') </th>
                                        <th> @lang('site.price') </th>
                                        <th> @lang('site.category') </th>
                                        <th> @lang('site.model') </th>
                                        <th> @lang('site.production_year') </th>
                                        <th> @lang('site.city') </th>
                                        <th> @lang('site.status') </th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td> {{\Illuminate\Support\Facades\App::getLocale() == "ar" ? $product->name_ar : $product->name_en}}</td>
                                        <td> {{ $product->provider->full_name }} </td>
                                        <td> {{\Illuminate\Support\Facades\App::getLocale() == "ar" ? $product->description_ar: $product->description_ar}}</td>
                                        <td> {{ $product->price }} </td>
                                        <td> {{\Illuminate\Support\Facades\App::getLocale() == "ar" ? $product->category->name_ar: $product->category->name_en}} </td>
                                        <td> {{ $product->model }} </td>
                                        <td> {{ $product->production_year }} </td>
                                        <td> {{\Illuminate\Support\Facades\App::getLocale() == "ar" ? $product->city->name_ar : $product->city->name_en}} </td>
                                        <td> {{ $product->status }} </td>
                                    </tr>

                                    </tbody>
                                </table>
                            @else

                                <h2> @lang('site.no_data_found') </h2>

                            @endif

                        </div>
                    </div>
                    <div class="tab-pane" id="timeline">
                        @if($product->count() > 0)

                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th> @lang('site.full_name') </th>
                                    <th> @lang('site.email') </th>
                                    <th> @lang('site.mobile') </th>
                                    <th> @lang('site.city') </th>
                                    <th> @lang('site.image') </th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td> {{$product->provider->full_name}}</td>
                                    <td> {{$product->provider->email}} </td>
                                    <td> {{$product->provider->mobile}}</td>
                                    <td>{{$product->provider->city->name_ar}} </td>
                                    {{--<td> {{\Illuminate\Support\Facades\App::getLocale() == "ar" ? $product->provider->city->name_ar : $product->provider->city->name_en}} </td>--}}
                                    <td><img src="{{ $product->provider->ImagePath }}" alt=""></td>
                                </tr>

                                </tbody>
                            </table>
                        @else

                            <h2> @lang('site.no_data_found') </h2>

                        @endif
                    </div>
                    <div class="tab-pane" id="settings">
                        @if($product->count() > 0)

                            <table class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th> @lang('site.rate_avg') </th>
                                    <th> @lang('site.views_num') </th>
                                    <th> @lang('site.like_count') </th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td> {{ $product->rate_avg }} </td>
                                    <td> {{ $product->views_num }} </td>
                                    <td> {{ $product->like_count }} </td>
                                </tr>

                                </tbody>
                            </table>
                        @else

                            <h2> @lang('site.no_data_found') </h2>

                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->


@endsection