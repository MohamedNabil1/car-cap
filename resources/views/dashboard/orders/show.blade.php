@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.order_details')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

        <li class="#"><a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
        <li class="active"><a href="{{ route('products.index') }}">@lang('site.products')</a></li>
    </ol>
@endsection

<div class="box box-primary">
    @include('dashboard.layouts.includes.errors')

    <div class="box-body">
        @if($order_products)

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th> #</th>
                    <th> @lang('site.order_number') </th>
                    <th> @lang('site.client_name') </th>
                    <th> @lang('site.product_name') </th>
                    <th> @lang('site.price') </th>
                    <th> @lang('site.tax_price') </th>
                    <th> @lang('site.delivery_price') </th>
                    <th> @lang('site.quantity') </th>
                    <th> @lang('site.total_price') </th>
                    <th> @lang('site.action') </th>
                </tr>
                </thead>
                <tbody>
                @foreach($order_products as $index=>$order_product )
                    <tr>
                        <td> {{ $index+1 }} </td>
                        <td> {{$order_product->order->order_number}}</td>
                        <td> {{$order_product->client->full_name}}</td>
                        <td> {{$order_product->product->$name}}</td>
                        <td> {{ $order_product->product_price }} </td>
                        <td> {{$order_product->product_tax_price}}</td>
                        <td> {{ $order_product->product_delivery_price }} </td>
                        <td> {{ $order_product->quantity }} </td>
                        <td> {{$order_product->product_total_price}} </td>
                        <td>
                            <a href=" {{route('products.show',$order_product->product)}} " class="btn btn-warning"> <i
                                        class="fas fa-eye"></i> @lang('site.product_details') </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <h2> @lang('site.no_data_found') </h2>
        @endif

    </div>
</div>


@endsection