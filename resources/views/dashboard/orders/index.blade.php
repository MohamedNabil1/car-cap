@extends('dashboard.layouts.master')
@section('content')

@section('header')
    <h1>
        @lang('site.dashboard')
        <small>@lang('site.control_panel') </small>
    </h1>
    <ol class="breadcrumb">

    </ol>
@endsection

<!-- Content Wrapper. Contains page content -->

<section class="content-header">
    <h1>
        @lang('site.all_orders')
    </h1>
    <ol class="breadcrumb">

        <li class="#"><i class="fa fa-dashboard"></i> <a
                    href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
        <li class="active"><a href="{{ route('users.index') }}">@lang('site.users')</a></li>
    </ol>
</section>

<section class="content">

    <div class="row">
        <div class="col-md-3">

            <!------------------------------------------------------- start about ---------------------------------------->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{auth()->user()->ImagePath}}"
                         alt="User profile picture">

                    <h3 class="profile-username text-center">{{auth()->user()->full_name}} </h3>

                    <p class="text-muted text-center">{{auth()->user()->type}} </p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>@lang('site.number_of_products')</b> <a class="pull-right">{{ $products->count()}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>@lang('site.number_of_orders')</b> <a class="pull-right">{{ $order->count() }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>@lang('site.user_type')</b> <a class="pull-right">{{ auth()->user()->type }}</a>
                        </li>
                    </ul>

                    {{--<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>--}}
                </div>
            </div>
            <div class="box box-primary">
            </div>
        </div>

        <!--------------------------------------------- end About -------------------------------------->

        <!------------------------------------ Tabs -------------------------------->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#pending_orders" data-toggle="tab">@lang('site.pending_orders')</a></li>
                    <li><a href="#on_processing" data-toggle="tab">@lang('site.on_processing_orders')</a></li>
                    <li><a href="#on_shipping" data-toggle="tab">@lang('site.on_shipping_orders')</a></li>
                    <li><a href="#delivered" data-toggle="tab">@lang('site.delivered_orders')</a></li>
                </ul>
                <div class="tab-content">

                    <!------------------------------------  Pending -------------------------------->
                    <div class="active tab-pane" id="pending_orders">
                        <div class="box-body">
                            @if(count($pending_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pending_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('process.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.process') </a>
                                                <a href=" {{route('ship.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.ship') </a>
                                                <a href=" {{route('deliver.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.delivered') </a>
                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>

                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.refuse') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End  Pending -------------------------------->


                    <!------------------------------------ On processing -------------------------------->
                    <div class="tab-pane" id="on_processing">
                        <div class="box-body">
                            @if(count($on_processing_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($on_processing_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('ship.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.ship') </a>
                                                <a href=" {{route('deliver.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.delivered') </a>

                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>
                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End On processing -------------------------------->


                    <!------------------------------------ On shipping -------------------------------->
                    <div class="tab-pane" id="on_shipping">
                        <div class="box-body">
                            @if(count($on_shipping_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($on_shipping_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('deliver.order',$order)}} " class="btn btn-info"> <i
                                                            class="fas fa-edit"></i> @lang('site.delivered') </a>
                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>
                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------End On shipping -------------------------------->


                    <!------------------------------------  Delivered  (Done) -------------------------------->
                    <div class="tab-pane" id="delivered">
                        <div class="box-body">
                            @if(count($delivered_orders) > 0)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> @lang('site.order_number') </th>
                                        <th> @lang('site.client_name') </th>
                                        <th> @lang('site.quantity') </th>
                                        <th> @lang('site.total_price') </th>
                                        {{--<th> @lang('site.order_status') </th>--}}
                                        <th> @lang('site.order_provider') </th>
                                        <th> @lang('site.action') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($delivered_orders as $index=>$order )
                                        <tr>
                                            <td> {{ $index+1 }} </td>
                                            <td> {{ $order->order_number }} </td>
                                            <td>{{ $order->client->full_name }}</td>
                                            <td> {{$order->total_quantity}}</td>
                                            <td> {{$order->total_order_price}}</td>
                                            {{--<td> {{$order->status}}</td>--}}
                                            <td> {{$order->provider->full_name}}</td>
                                            <td>
                                                <a href=" {{route('orders.show',$order)}} "
                                                   class="btn btn-warning btn-sm"> <i
                                                            class="fas fa-eye"></i> @lang('site.order_details') </a>
                                                <form action=" {{ route('orders.destroy',$order->id) }} " method="POST"
                                                      style="display:inline-block">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger delete"><i
                                                                class="fa fa-trash"></i> @lang('site.delete') </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                    <!------------------------------------ Delivered  -------------------------------->


                </div>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->

@endsection