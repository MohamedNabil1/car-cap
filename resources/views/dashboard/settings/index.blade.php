@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.products')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('products.index') }}">@lang('site.products')</a></li>
</ol>
@endsection

<div class="box box-primary">
    <div class="panel-body">
        @include('dashboard.layouts.includes.errors')

        <form action="{{ route('settings.update') }}" method="post">
            @csrf

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.app_name_ar') }} <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" value="{{ SETTING_VALUE('APP_NAME_AR') }}" name="APP_NAME_AR" class="form-control" required="required" placeholder="{{ trans('site.app_name_ar') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.app_name_en') }} <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" value="{{ SETTING_VALUE('APP_NAME_EN') }}" name="APP_NAME_EN" class="form-control" required="required" placeholder="{{ trans('site.app_name_en') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.about_app_ar') }} <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <textarea rows="5" cols="5" name="ABOUT_AR" class="form-control" required="required" placeholder="{{ trans('site.about_app_ar') }}"> {{ SETTING_VALUE('ABOUT_AR') }} </textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.about_app_en') }} <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <textarea rows="5" cols="5" name="ABOUT_EN" class="form-control" required="required" placeholder="{{ trans('site.about_app_en') }}"> {{ SETTING_VALUE('ABOUT_EN') }} </textarea>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.privacy_policy_ar') }} </label>
                <div class="col-lg-9">
                    <textarea rows="5" cols="5" class="form-control" name="PRIVACY_POLICY_AR" placeholder="{{ trans('site.privacy_policy_ar') }}"> {{ SETTING_VALUE('PRIVACY_POLICY_AR') }} </textarea>
                </div>

            </div>

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.privacy_policy_en') }} </label>
                <div class="col-lg-9">
                    <textarea rows="5" cols="5" class="form-control" name="PRIVACY_POLICY_EN" placeholder="{{ trans('site.privacy_policy_en') }}"> {{ SETTING_VALUE('PRIVACY_POLICY_EN') }} </textarea>
                </div>

            </div>


            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.terms_ar') }} </label>
                <div class="col-lg-9">
                    <textarea rows="5" cols="5" class="form-control" name="TERMS_AR" placeholder="{{ trans('site.terms_ar') }}"> {{ SETTING_VALUE('TERMS_AR') }} </textarea>
                </div>

            </div>

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.terms_en') }} </label>
                <div class="col-lg-9">
                    <textarea rows="5" cols="5" class="form-control" name="TERMS_EN" placeholder="{{ trans('site.terms_en') }}"> {{ SETTING_VALUE('TERMS_EN') }} </textarea>
                </div>

            </div>

            <div class="form-group">
                <label class="control-label col-lg-3"> {{ trans('site.app_percentage') }} % <span class="text-danger">*</span></label>
                <div class="col-lg-9">
                    <input type="text" value="{{ SETTING_VALUE('APP_PERCENTAGE') }}" name="APP_PERCENTAGE" class="form-control" required="required" placeholder="{{ trans('site.app_percentage') }}">
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-success" type="submit"> {{trans('site.edit')}} </button>
            </div>

        </form>



    </div>
</div>




@endsection