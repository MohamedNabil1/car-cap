@extends('dashboard.layouts.master')
@section('content')

@section('header')
<h1>
    @lang('site.privacy_policy')
    <small>@lang('site.control_panel') </small>
</h1>
<ol class="breadcrumb">

    <li class="#"> <a href="{{ route('dashboard.index') }}">@lang('site.dashboard')</a></li>
    <li class="active"><a href="{{ route('products.index') }}">@lang('site.products')</a></li>
</ol>
@endsection

<div class="box box-primary">
    <div class="panel-body">
        @include('dashboard.layouts.includes.errors')

        @if($privacy)

        <p> $privacy </p>

        @else

        <h2> @lang('site.no_data_found') </h2>

        @endif



    </div>
</div>





@endsection