<!DOCTYPE html>
<html>

<head>
    @include('dashboard.layouts.includes.head')
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        @include('dashboard.layouts.includes.header')
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        @include('dashboard.layouts.includes.aside')
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('header')
        </section>

        <section class="content-header">
            <h1>لوحة تحكم Car Cap</h1>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Info boxes -->
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fas fa-city"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{{ trans('site.cities') }}</span>
                            <span class="info-box-number">{{ $cities ? count($cities) : '0' }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="ion ion-ios-cart-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{{ trans('site.orders') }}</span>
                            <span class="info-box-number">{{ $orders ? count($orders) : '0' }}</span>
                        </div>
                    </div>
                </div>

                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fas fa-business-time"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{{trans('site.products')}}</span>
                            <span class="info-box-number">{{ $all_products  ? count($all_products) : '0'}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">{{trans('site.users')}}</span>
                            <span class="info-box-number">{{ $users->count() }}</span>
                        </div>
                    </div>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">{{trans('site.providers')}}</span>
                            <span class="info-box-number">{{ $providers->count() }}</span>
                        </div>

                    </div>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fas fa-business-time"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">{{trans('site.categories')}}</span>
                            <span class="info-box-number">{{ $categories  ? count($categories) : '0'}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-aqua"><i class="fas fa-city"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">{{ trans('site.advertises') }}</span>
                            <span class="info-box-number">{{ $advertises ? count($advertises) : '0' }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{trans('site.last_products')}}</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            @if($latest_products)
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center"> #</th>
                                        <th class="text-center"> @lang('site.name_ar') </th>
                                        <th class="text-center"> @lang('site.owner') </th>
                                        <th class="text-center"> @lang('site.description_ar') </th>
                                        <th class="text-center"> @lang('site.price') </th>
                                        <th class="text-center"> @lang('site.category') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($latest_products as $index=>$product )
                                        <tr>
                                            <td class="text-center"> {{ $index+1 }} </td>
                                            <td class="text-center"> {{$product->$name}}</td>
                                            <td class="text-center"> {{ $product->provider->full_name }} </td>
                                            <td class="text-center"> {{$product->$description}}</td>
                                            <td class="text-center"> {{ $product->price }} </td>
                                            <td class="text-center"> {{$product->category->$name ?? ''}} </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">{{trans('site.most_products_ordered')}}</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-box-tool" data-widget="remove"><i
                                            class="fa fa-times"></i></button>
                            </div>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">

                            @if($most_products)

                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="text-center"> #</th>
                                        <th class="text-center"> @lang('site.name_ar') </th>
                                        <th class="text-center"> @lang('site.owner') </th>
                                        <th class="text-center"> @lang('site.description_ar') </th>
                                        <th class="text-center"> @lang('site.price') </th>
                                        <th class="text-center"> @lang('site.category') </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($most_products as $index=>$product )
                                        <tr>
                                            <td class="text-center"> {{ $index+1 }} </td>
                                            <td class="text-center"> {{$product->$name}}</td>
                                            <td class="text-center"> {{ $product->provider->full_name }} </td>
                                            <td class="text-center"> {{$product->$description}}</td>
                                            <td class="text-center"> {{ $product->price }} </td>
                                            <td class="text-center"> {{$product->category->$name ?? ''}} </td>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <h2> @lang('site.no_data_found') </h2>
                            @endif
                        </div>

                    </div>
                </div>
            </div>

    </div>

    </section>

</div>


<footer class="main-footer">
    @include('dashboard.layouts.includes.footer')
</footer>


@include('dashboard.layouts.includes.scripts')
</body>

</html>