<?php

return [
    'address_not_found' => 'لم يتم العثور على هذا العنوان',
    'user_not_fount' => 'لم يتم العثور على هذا المستخدم',
    'no_data_found' => 'لم يتم العثور على اى بيانات',
    'updated_successfully' => 'تم التعديل  بنجاح',
    'deleted_successfully' => 'تم الحذف  بنجاح',

    'pending_order' => 'تم   الطلب بنجاح انتظر موافقه موفر الخدمه',
    'cant_deleted_add_more_first' => 'لا يمكنك الحذف اضف المزيد اولا',
];
