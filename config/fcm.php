<?php

return [
    'driver' => env('FCM_PROTOCOL', 'http'),
    'log_enabled' => false,

    'http' => [
        'server_key' => env('FCM_SERVER_KEY', 'AAAATDEChBk:APA91bEvq4jQi2fDk08tYjPt6v2El7823MYHac6NAu_d10fS9kAAVSA-wxQu4z7JC-QMOqhNh1t2YpR4t6aOMOuvkABHmyoKTqFk0BDTSs89FZPz9tw6JaO4CMuur-zu-2-JKikmFrwL'),
        'sender_id' => env('FCM_SENDER_ID', '327239762969'),
        'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
        'server_group_url' => 'https://android.googleapis.com/gcm/notification',
        'timeout' => 30.0, // in second
    ],
];
