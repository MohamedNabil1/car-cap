<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('full_name');
            $table->boolean('register_complete')->default(0);
            $table->string('email')->unique();
            $table->bigInteger('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('CASCADE')->onUpdate('CASCADE');
            $table->string('reset_code')->default('');
            $table->string('code')->default('');
            $table->integer('rates')->default(0);
            $table->integer('tax_price')->default(1);  //%
            $table->integer('delivery_price')->default(1);
            $table->enum('active', ['active', 'deactive', 'wait_admin_confirm'])->default('deactive');
            $table->string('latitude')->default('');
            $table->string('longitude')->default('');
            $table->enum('gender', ['male', 'female'])->default('male');
            $table->string('image')->default('');
            $table->string('type')->nullable();
            $table->string('mobile')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('area')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
