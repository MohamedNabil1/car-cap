<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_id');
            $table->integer('provider_id');
            $table->bigInteger('product_id');
            $table->string('product_name_ar');
            $table->string('product_name_en');
            $table->string('product_category_id');
            $table->string('product_subcategory_id');
            $table->string('product_image');
            $table->double('product_price');
            $table->double('product_tax_price');
            $table->double('product_delivery_price');
            $table->double('product_total_price');
            $table->bigInteger('quantity')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bags');
    }
}
