<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'first_name' => 'super',
            'last_name' => 'admin',
            'full_name' => 'super admin',
            'email' => 'super_admin@app.com',
            'password' => bcrypt('123456'),
            'code' => '123',
            'active' => 'active',
            'type' => 'admin',
            'mobile' => '123',
        ]);

        $user->attachRole('super_admin');
    }
}
